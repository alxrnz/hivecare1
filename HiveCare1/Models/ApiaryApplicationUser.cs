﻿using HiveCare1.Models.Partials;

namespace HiveCare1.Models
{
    public class ApiaryApplicationUser
    {
        public string ApplicationUserId { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }
        public int ApiaryId { get; set; }
        public virtual Apiary Apiary { get; set; }
        public bool IsAdmin { get; set; }
    }
}