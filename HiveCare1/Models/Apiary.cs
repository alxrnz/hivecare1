﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using HiveCare1.Models.Partials;

namespace HiveCare1.Models
{
    public class Apiary : CreatorPartial,IUserDevider
    {
        [Key] public int ApiaryId { get; set; }
        public string Name { get; set; }
        
        public virtual ICollection<Location> Locations { get; set; }

        public virtual ICollection<ApiaryApplicationUser> Users { get; set; } = new List<ApiaryApplicationUser>();

        [NotMapped] public List<ApplicationUser> Admins { get => Users?.Where(x => x.IsAdmin).Select(x => x.ApplicationUser).ToList(); }
        [NotMapped] public List<ApplicationUser> Beekeepers { get => Users?.Where(x => !x.IsAdmin).Select(x => x.ApplicationUser).ToList(); }
        [NotMapped] public List<HiveYieldAmount> Yields { get => Locations?.SelectMany(x => x.Hives).SelectMany(hive => hive.YieldAmounts).ToList(); }
    }

}