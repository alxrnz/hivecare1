﻿using System.Collections.Generic;
using System.Linq;

namespace HiveCare1.Models
{
    public class BeekeepingModel
    {
        public List<Apiary> Apiaries { get; set; } = new List<Apiary>();
        public List<Hive> Hives { get; set; } = new List<Hive>();
        public List<Location> Locations { get; set; } = new List<Location>();

        public BeekeepingModel(List<Apiary> apiaries)
        {
            Apiaries = apiaries;
            foreach (var locationlist in apiaries.Select(a=>a.Locations))
            {
                Locations.AddRange(locationlist);
                foreach (var location in locationlist)
                {
                    Hives.AddRange(location.Hives);
                }
            }
        }
    }
}