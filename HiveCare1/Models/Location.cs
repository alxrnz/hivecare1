﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using HiveCare1.Data;
using HiveCare1.Models.Partials;

namespace HiveCare1.Models
{
    public class Location : AddressPartial, IUserDevider
    {
        [Key] public int LocationId { get; set; }
        [NotMapped] public int ApiaryId { get; set; }
        public virtual Apiary Apiary { get; set; }
        public string Name { get; set; }
        public virtual ICollection<Hive> Hives { get; set; } = new List<Hive>();
        public virtual ICollection<LocationApplicationUser> Users { get; set; } = new List<LocationApplicationUser>();
        
        [NotMapped] public List<ApplicationUser> Admins
        {
            get
            {
                return Users.Where(x => x.IsAdmin).Select(x => x.ApplicationUser).ToList();
            }
        }

        [NotMapped] public List<ApplicationUser> Beekeepers { get => Users.Where(x => !x.IsAdmin).Select(x => x.ApplicationUser).ToList(); }
    }
}