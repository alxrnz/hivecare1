﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Security.Cryptography.X509Certificates;
using HiveCare1.Models.Partials;

namespace HiveCare1.Models
{
    public enum FeedTypeEnum
    {
        Zuckerwasser,
        Sirup,
        Futterteig
    }
    
    public class Feed : Job
    {
        private static Dictionary<FeedTypeEnum, string> measure = new Dictionary<FeedTypeEnum,string>(new []
        {
            new KeyValuePair<FeedTypeEnum,string>(FeedTypeEnum.Futterteig,"kg"), 
            new KeyValuePair<FeedTypeEnum,string>(FeedTypeEnum.Sirup,"l"), 
            new KeyValuePair<FeedTypeEnum,string>(FeedTypeEnum.Zuckerwasser,"l"), 
        });
        public int FeedId { get; set; }

        [Display(Name = "Futterart")] public FeedTypeEnum FeedType { get; set; }

        public double Amout { get; set; }
        public string Comment { get; set; }
        [NotMapped] public string GetAmount => Amout.ToString(CultureInfo.InvariantCulture) + measure[FeedType];
    }
}