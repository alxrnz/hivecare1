﻿using System.Collections.Generic;

namespace HiveCare1.Models
{
    public class ManageApplicationUsers
    {
        public List<ApplicationUser> UserList { get; set; }
    }
}