﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using HiveCare1.Models.Partials;

namespace HiveCare1.Models
{
    public class Diary : CreatorPartial
    {
        public int DiaryId { get; set; }

        public string Description { get; set; }

        public string Text { get; set; }
    }
}