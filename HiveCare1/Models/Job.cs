﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using HiveCare1.Models.Partials;

namespace HiveCare1.Models
{
    public class Job : CreatorPartial
    {
        public virtual Hive Hive { get; set; }
        [NotMapped] public int HiveId { get; set; }

        [DataType(DataType.DateTime)] public DateTime DueDateTime { get; set; }
        [DataType(DataType.DateTime)] public DateTime DoneDateTime { get; set; }
        public virtual ApplicationUser DoneBy { get; set; }
        public string DoneById { get; set; }
        public bool IsDone { get; set; } = false;
        [NotMapped] public bool IsPlanned => !IsDone;
        [NotMapped] public bool IsElapsed => DueDateTime < DateTime.Now;
        [NotMapped] public bool IsElapsedDone => DueDateTime < DoneDateTime;
        
    }
}
