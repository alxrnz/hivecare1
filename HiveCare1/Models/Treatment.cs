﻿using System.ComponentModel.DataAnnotations;
using HiveCare1.Models;
using HiveCare1.Models.Partials;

namespace HiveCare1.Models
{
    public enum TreatmentAgenttypeEnum {
        Ameisensäure,
        Oxalsäure,
        Milchsäure
    }
    public class Treatment : Job
    {
        public int TreatmentId { get; set; }
        [Display(Name = "Behandlungsmittel")] public TreatmentAgenttypeEnum TypeOfAgenttype { get; set; }

        [Display(Name = "Menge")] public double Amount { get; set; }
        [Display(Name = "Brut")] public bool Brood { get; set; }

        [Display(Name = "Stifte")] public bool BeePen { get; set; }

        public bool Varroa { get; set; }
        [Display(Name = "Gewicht")] public double Weight { get; set; }
        [Display(Name = "Notiz")] public string Comment { get; set; }
    }
}