﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using HiveCare1.Data;
using HiveCare1.Models.Partials;

namespace HiveCare1.Models
{
    public enum HiveTypeEnum{ Jungvolk,Wirtschaftsvolk, Ableger, Schwarm, Aufgelöst }
    public class Hive : CreatorPartial, IUsers<HiveApplicationUser>,IUserDevider
    {
        [Key] public int HiveId { get; set; }
        public virtual ICollection<HiveApplicationUser> Users { get; set; } = new List<HiveApplicationUser>();
        public string Name { get; set; }
        public HiveTypeEnum Type { get; set; }
        public virtual Queen Queen { get; set; }
        public virtual Apiary Apiary { get; set; }
        [NotMapped]
        public int ApiaryId { get; set; }
        [NotMapped]
        public int QueenId { get; set; }

        [DataType("range")]
        [Display(Name = "Sanftmut")]
        public int Gentleness { get; set; }

        [DataType("range")]
        [Display(Name = "Wabentreue")]
        public int Loyalty { get; set; }

        [DataType("range")]
        [Display(Name = "Schwarmträgheit")]
        public int Inertia { get; set; }

        [DataType("range")]
        [Display(Name = "Stechlust")]
        public int Aggressiveness { get; set; }

        public virtual Location Location { get; set; }

        public virtual ICollection<Observation> Observations { get; set; } = new List<Observation>();

        [Display(Name = "Tätigkeit")]
        public virtual ICollection<Process> Processes { get; set; } = new List<Process>();

        public virtual ICollection<Treatment> Treatments { get; set; } = new List<Treatment>();
        public virtual ICollection<Feed> Feeds { get; set; } = new List<Feed>();
        public virtual ICollection<HiveYieldAmount> YieldAmounts { get; set; } = new List<HiveYieldAmount>();

        [NotMapped] public int LocationId { get; set; }

        [NotMapped] public double SumYields => YieldAmounts.Sum(x => x.Yield.Amount);

        [NotMapped] public List<ApplicationUser> Admins { get => Users.Where(x=>x.IsAdmin).Select(x => x.ApplicationUser).ToList(); }
        [NotMapped] public List<ApplicationUser> Beekeepers { get => Users.Where(x => !x.IsAdmin).Select(x => x.ApplicationUser).ToList(); }
    }

    public interface IUsers<T>
    {
       ICollection<T> Users { get; set; }
    }
}