﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using HiveCare1.Data;
using HiveCare1.Models.Partials;

namespace HiveCare1.Models
{
    public enum YieldTypeEnum
    {
        Blütenhonig,
        Waldhonig,
        Sortenhonig 
    }
    public class Yield : CreatorPartial
    {
        public int YieldId { get; set; }
        public virtual ICollection<HiveYieldAmount> Amounts { get; set; } = new List<HiveYieldAmount>();

        [NotMapped]
        [Display(Name = "Anzahl Honigwaben")]
        public int HoneycombCount => Amounts.Sum(x => x.HoneycombCount);

        [NotMapped]
        [Display(Name = "Menge (kg)")]
        public double Amount => Amounts.Sum(x => x.Amount);

        [NotMapped]
        [Display(Name = "Wassergehalt (%)")]
        public int WaterShare => (Amounts.Sum(x => x.WaterShare) / (Amounts.Count != 0 ? Amounts.Count : 1));

        [Display(Name = "Art des Honigs")] public YieldTypeEnum YieldType { get; set; }
        [Display(Name = "Sortenhonig Text")] public string YieldTypeText { get; set; }
    }
}