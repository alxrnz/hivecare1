﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HiveCare1.Models.Partials
{
    public interface IUserDevider
    {
        [NotMapped] List<ApplicationUser> Admins { get; }
        [NotMapped] List<ApplicationUser> Beekeepers { get; }
    }
}
