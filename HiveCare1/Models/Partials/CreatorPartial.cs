﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HiveCare1.Models.Partials
{
    public class CreatorPartial
    {
        public virtual ApplicationUser Creator { get; set; }

        [NotMapped] public string CreatorId { get; set; }

        [DataType(DataType.DateTime)]
        [Display(Name = "Creation Date")]

        public DateTime CreationDate { get; set; }
    }
}