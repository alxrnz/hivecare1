﻿using System.ComponentModel;

namespace HiveCare1.Models.Partials
{
    public class AddressPartial : CreatorPartial
    { 
        [DisplayName("Straße")] public string Street { get; set; }

        [DisplayName("Hausnummer")] public string HouseNo { get; set; }

        [DisplayName("Stadt")] public string City { get; set; }

        [DisplayName("PLZ")] public string Zip { get; set; }

        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}