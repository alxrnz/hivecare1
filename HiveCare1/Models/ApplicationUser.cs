﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using HiveCare1.Data;
using Microsoft.AspNetCore.Identity;

namespace HiveCare1.Models
{
    public class ApplicationUser : IdentityUser
    {
        public string Password { get; set; }

        [PersonalData] public string Street { get; set; }

        [PersonalData] public string HouseNo { get; set; }

        [PersonalData] public string City { get; set; }

        [PersonalData] public string Zip { get; set; }

        [PersonalData] public string Firstname { get; set; }

        [PersonalData] public string Familyname { get; set; }

        public virtual ApplicationUser Creator { get; set; }

        [NotMapped] public string CreatorId { get; set; }
        [DataType(DataType.DateTime)]
        [Display(Name = "Erstellt am")]
        public DateTime CreationDate { get; set; }

        public virtual ICollection<HiveApplicationUser> Hives { get; set; } = new List<HiveApplicationUser>();
        public virtual ICollection<LocationApplicationUser> Locations { get; set; } = new List<LocationApplicationUser>();
        public virtual ICollection<ApiaryApplicationUser> Apiaries { get; set; } = new List<ApiaryApplicationUser>();

        [NotMapped] public string Roles { get; set; }
    }
}