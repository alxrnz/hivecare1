﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using HiveCare1.Models.Partials;

namespace HiveCare1.Models
{
    public enum AmountValueEnum
    {
        Viel,
        Wenig,
        Kein,
    }
    public class Observation : Job
    {
        public int ObservationId { get; set; }
        [Display(Name = "Weiselzelle")] public bool QueenCells { get; set; }
        [Display(Name = "Spielnäpfchen")] public bool Pans { get; set; }
        [Display(Name = "Futterwaben")] public AmountValueEnum Liningcells { get; set; }

        [Display(Name = "Königin")] public bool Queen { get; set; }
       
        [Display(Name = "Brut")] public bool Brood { get; set; }

        [Display(Name = "Stifte")] public bool Beepen { get; set; }

        [Display(Name = "Varrora")] public bool Varrora { get; set; }

        [Display(Name = "Polleneintrag")] public bool PollenEntry { get; set; }

        [Display(Name = "Flugverkehr")] public AmountValueEnum AirTraffic { get; set; }
        [Display(Name="Temperatur")] public double Temperature { get; set; }
        [Display(Name="Weather")] public string Weather { get; set; }
        
        [Display(Name = "Milbenfall")] public int MiteInfestation { get; set; }

        [Display(Name = "Gewicht")] public double Weight { get; set; }
        [Display(Name = "Notiz")] public string Comment { get; set; }
    }
}