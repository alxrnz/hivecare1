﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HiveCare1.Models
{
    public class HiveYieldAmount : Job
    {
        [Key] public int HiveYieldAmountId { get; set; }
        public virtual Yield Yield { get; set; }
        [Display(Name = "Anzahl Honigwaben")] public int HoneycombCount { get; set; }

        [Display(Name = "Menge (kg)")] public double Amount { get; set; } = 0.0;


        [Display(Name = "Wassergehalt (%)")] public int WaterShare { get; set; } = 0;
        [NotMapped]public int YieldId { get; set; }
    }
}