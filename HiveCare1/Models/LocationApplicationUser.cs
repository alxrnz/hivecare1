﻿using HiveCare1.Models;

namespace HiveCare1.Data
{
    public class LocationApplicationUser
    {
        public int LocationId { get; set; }
        public virtual Location Location { get; set; }
        public string ApplicationUserId { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }

        public bool IsAdmin { get; set; }= false;
    }
}