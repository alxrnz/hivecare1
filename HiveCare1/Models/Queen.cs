﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using HiveCare1.Models.Partials;

namespace HiveCare1.Models
{
    [Display(Name = "Königin")]
    public class Queen : CreatorPartial
    {
        [Display(Name = "Bienenvolk")]
        public int HiveId { get; set; }

        [Range(1990, 9000)]
        [Display(Name = "Jahr")]
        public int QueenBirthYear { get; set; }

        [Display(Name = "Ist gezeichnet")]
        public bool QueenPainted { get; set; }

        public int QueenId { get; set; }
        public virtual Hive Hive { get; set; }
        [DataType("number")]
        [Display(Name = "Nummer")] public int QueenNo { get; set; }
    }
}
