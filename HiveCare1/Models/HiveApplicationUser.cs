﻿namespace HiveCare1.Models
{
    public class HiveApplicationUser
    {
        public string ApplicationUserId { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }

        public int HiveId { get; set; }
        public virtual Hive Hive { get; set; }

        public bool IsAdmin { get; set; } = false;
    }
}