﻿using System.ComponentModel.DataAnnotations;
using HiveCare1.Models.Partials;
using Microsoft.AspNetCore.Authentication;

namespace HiveCare1.Models
{
    public enum FlightholeStatusEnum
    {
        Closed,
        Small,
        Big,
        None
    }

    public class Process : Job
    {        
        public int ProcessId { get; set; }
        [Display(Name = "Drohnenrahmen")] public int DronesFrames { get; set; } = 0;
        [Display(Name = "Brutrahmen")] public int BrutFrames { get; set; } = 0;
        [Display(Name = "Leerrahmen")] public int EmptyFrames { get; set; } = 0;
        [Display(Name = "Futterrahmen")] public int LiningFrames { get; set; } = 0;
        [Display(Name = "Mittelwandrahmen")] public int MiddlewallFrame { get; set; } = 0;
        [Display(Name = "Honigzarge")] public int HoneyBox { get; set; } = 0;
        [Display(Name = "Leerzarge")] public int EmptyBox { get; set; } = 0;
        [Display(Name = "Bienenflucht")] public bool BeesEscape { get; set; } = false;
        [Display(Name = "Absperrgitter")] public bool Fence { get; set; } = false;
        [Display(Name = "Windel")] public bool Diaper { get; set; }
        [Display(Name = "Fluglochkeil")] public FlightholeStatusEnum FlightholeWedge { get; set; }
        [Display(Name = "Freier Text")] public string Comment { get; set; }
    }
}