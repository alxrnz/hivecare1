﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using HiveCare1.Models;
using HiveCare1.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace HiveCare1
{
    public class ApplicationUserManager : UserManager<ApplicationUser>
    {
        private readonly IAuthorizationService _authorizationService;
        private readonly UserService _userService;
        public ApplicationUserManager(
            IUserStore<ApplicationUser> store,
            IOptions<IdentityOptions> optionsAccessor,
            IPasswordHasher<ApplicationUser> passwordHasher,
            IEnumerable<IUserValidator<ApplicationUser>> userValidators,
            IEnumerable<IPasswordValidator<ApplicationUser>> passwordValidators,
            ILookupNormalizer keyNormalizer,
            IdentityErrorDescriber errors,
            IServiceProvider services,
            ILogger<ApplicationUserManager> logger,
            UserService userService, IAuthorizationService authorizationService)
            : base(store, optionsAccessor, passwordHasher, userValidators, passwordValidators, keyNormalizer, errors, services, logger)
        {
            _userService = userService;
            _authorizationService = authorizationService;
        }

        public override Task<ApplicationUser> GetUserAsync(ClaimsPrincipal principal)
        {
            return base.GetUserAsync(principal);
        }
        public async Task<ApplicationUser> GetCurrentUserAsync()
        {
            var user = _userService.GetUser();
            var _user = await Users.Include(u => u.Apiaries).ThenInclude(a => a.Apiary).ThenInclude(a=>a.Locations).ThenInclude(l=>l.Hives)
                .Include(u => u.Locations).ThenInclude(a => a.Location).ThenInclude(l=>l.Hives).ThenInclude(h => h.Users)
                .Include(u => u.Hives).ThenInclude(h => h.Hive).ThenInclude(hive => hive.YieldAmounts).FirstOrDefaultAsync(u => u.UserName == user.Identity.Name);
            return _user;
        }

        public override Task<ApplicationUser> FindByEmailAsync(string email)
        {
            return base.FindByEmailAsync(email);
        }

        public override Task<IList<ApplicationUser>> GetUsersInRoleAsync(string roleName)
        {
            return base.GetUsersInRoleAsync(roleName);
        }
        
        public new virtual IQueryable<ApplicationUser> Users
        {
            get
            {
                IQueryableUserStore<ApplicationUser> store = this.Store as IQueryableUserStore<ApplicationUser>;
                if (store != null)
                    return store.Users.Include(user => user.Apiaries)
                        .Include(user => user.Locations)
                        .Include(user => user.Hives)
                        .Include(user => user.Creator);
                throw new Exception("cant get users");
            }
        }
    }
}
