﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HiveCare1.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    Password = table.Column<string>(nullable: true),
                    Street = table.Column<string>(nullable: true),
                    HouseNo = table.Column<string>(nullable: true),
                    City = table.Column<string>(nullable: true),
                    Zip = table.Column<string>(nullable: true),
                    Firstname = table.Column<string>(nullable: true),
                    Familyname = table.Column<string>(nullable: true),
                    CreatorId1 = table.Column<string>(nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUsers_AspNetUsers_CreatorId1",
                        column: x => x.CreatorId1,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    RoleId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Apiaries",
                columns: table => new
                {
                    ApiaryId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    CreatorId1 = table.Column<string>(nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Apiaries", x => x.ApiaryId);
                    table.ForeignKey(
                        name: "FK_Apiaries_AspNetUsers_CreatorId1",
                        column: x => x.CreatorId1,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    UserId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    RoleId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Diaries",
                columns: table => new
                {
                    DiaryId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    CreatorId1 = table.Column<string>(nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    Text = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Diaries", x => x.DiaryId);
                    table.ForeignKey(
                        name: "FK_Diaries_AspNetUsers_CreatorId1",
                        column: x => x.CreatorId1,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Yields",
                columns: table => new
                {
                    YieldId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    CreatorId1 = table.Column<string>(nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    YieldType = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    YieldTypeText = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Yields", x => x.YieldId);
                    table.ForeignKey(
                        name: "FK_Yields_AspNetUsers_CreatorId1",
                        column: x => x.CreatorId1,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ApiaryApplicationUsers",
                columns: table => new
                {
                    ApplicationUserId = table.Column<string>(nullable: false),
                    ApiaryId = table.Column<int>(nullable: false),
                    IsAdmin = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ApiaryApplicationUsers", x => new { x.ApiaryId, x.ApplicationUserId });
                    table.ForeignKey(
                        name: "FK_ApiaryApplicationUsers_Apiaries_ApiaryId",
                        column: x => x.ApiaryId,
                        principalTable: "Apiaries",
                        principalColumn: "ApiaryId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ApiaryApplicationUsers_AspNetUsers_ApplicationUserId",
                        column: x => x.ApplicationUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Locations",
                columns: table => new
                {
                    LocationId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    CreatorId1 = table.Column<string>(nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    Street = table.Column<string>(nullable: true),
                    HouseNo = table.Column<string>(nullable: true),
                    City = table.Column<string>(nullable: true),
                    Zip = table.Column<string>(nullable: true),
                    Latitude = table.Column<double>(nullable: false),
                    Longitude = table.Column<double>(nullable: false),
                    ApiaryId1 = table.Column<int>(nullable: true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Locations", x => x.LocationId);
                    table.ForeignKey(
                        name: "FK_Locations_Apiaries_ApiaryId1",
                        column: x => x.ApiaryId1,
                        principalTable: "Apiaries",
                        principalColumn: "ApiaryId",
                        onDelete: ReferentialAction.SetNull);
                    table.ForeignKey(
                        name: "FK_Locations_AspNetUsers_CreatorId1",
                        column: x => x.CreatorId1,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Hives",
                columns: table => new
                {
                    HiveId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    CreatorId1 = table.Column<string>(nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Type = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    ApiaryId1 = table.Column<int>(nullable: true),
                    Gentleness = table.Column<int>(nullable: false),
                    Loyalty = table.Column<int>(nullable: false),
                    Inertia = table.Column<int>(nullable: false),
                    Aggressiveness = table.Column<int>(nullable: false),
                    LocationId1 = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Hives", x => x.HiveId);
                    table.ForeignKey(
                        name: "FK_Hives_Apiaries_ApiaryId1",
                        column: x => x.ApiaryId1,
                        principalTable: "Apiaries",
                        principalColumn: "ApiaryId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Hives_AspNetUsers_CreatorId1",
                        column: x => x.CreatorId1,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Hives_Locations_LocationId1",
                        column: x => x.LocationId1,
                        principalTable: "Locations",
                        principalColumn: "LocationId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "LocationApplicationUsers",
                columns: table => new
                {
                    LocationId = table.Column<int>(nullable: false),
                    ApplicationUserId = table.Column<string>(nullable: false),
                    IsAdmin = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LocationApplicationUsers", x => new { x.LocationId, x.ApplicationUserId });
                    table.ForeignKey(
                        name: "FK_LocationApplicationUsers_AspNetUsers_ApplicationUserId",
                        column: x => x.ApplicationUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_LocationApplicationUsers_Locations_LocationId",
                        column: x => x.LocationId,
                        principalTable: "Locations",
                        principalColumn: "LocationId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Feeds",
                columns: table => new
                {
                    FeedId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    CreatorId1 = table.Column<string>(nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    HiveId1 = table.Column<int>(nullable: true),
                    DueDateTime = table.Column<DateTime>(nullable: false),
                    DoneDateTime = table.Column<DateTime>(nullable: false),
                    DoneById = table.Column<string>(nullable: true),
                    IsDone = table.Column<bool>(nullable: false),
                    FeedType = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Amout = table.Column<double>(nullable: false),
                    Comment = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Feeds", x => x.FeedId);
                    table.ForeignKey(
                        name: "FK_Feeds_AspNetUsers_CreatorId1",
                        column: x => x.CreatorId1,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Feeds_AspNetUsers_DoneById",
                        column: x => x.DoneById,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Feeds_Hives_HiveId1",
                        column: x => x.HiveId1,
                        principalTable: "Hives",
                        principalColumn: "HiveId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "HiveApplicationUsers",
                columns: table => new
                {
                    ApplicationUserId = table.Column<string>(nullable: false),
                    HiveId = table.Column<int>(nullable: false),
                    IsAdmin = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HiveApplicationUsers", x => new { x.ApplicationUserId, x.HiveId });
                    table.ForeignKey(
                        name: "FK_HiveApplicationUsers_AspNetUsers_ApplicationUserId",
                        column: x => x.ApplicationUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_HiveApplicationUsers_Hives_HiveId",
                        column: x => x.HiveId,
                        principalTable: "Hives",
                        principalColumn: "HiveId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "HiveYieldAmounts",
                columns: table => new
                {
                    HiveYieldAmountId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    CreatorId1 = table.Column<string>(nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    HiveId1 = table.Column<int>(nullable: true),
                    DueDateTime = table.Column<DateTime>(nullable: false),
                    DoneDateTime = table.Column<DateTime>(nullable: false),
                    DoneById = table.Column<string>(nullable: true),
                    IsDone = table.Column<bool>(nullable: false),
                    YieldId1 = table.Column<int>(nullable: true),
                    HoneycombCount = table.Column<int>(nullable: false),
                    Amount = table.Column<double>(nullable: false),
                    WaterShare = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HiveYieldAmounts", x => x.HiveYieldAmountId);
                    table.ForeignKey(
                        name: "FK_HiveYieldAmounts_AspNetUsers_CreatorId1",
                        column: x => x.CreatorId1,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_HiveYieldAmounts_AspNetUsers_DoneById",
                        column: x => x.DoneById,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_HiveYieldAmounts_Hives_HiveId1",
                        column: x => x.HiveId1,
                        principalTable: "Hives",
                        principalColumn: "HiveId",
                        onDelete: ReferentialAction.SetNull);
                    table.ForeignKey(
                        name: "FK_HiveYieldAmounts_Yields_YieldId1",
                        column: x => x.YieldId1,
                        principalTable: "Yields",
                        principalColumn: "YieldId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Observations",
                columns: table => new
                {
                    ObservationId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    CreatorId1 = table.Column<string>(nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    HiveId1 = table.Column<int>(nullable: true),
                    DueDateTime = table.Column<DateTime>(nullable: false),
                    DoneDateTime = table.Column<DateTime>(nullable: false),
                    DoneById = table.Column<string>(nullable: true),
                    IsDone = table.Column<bool>(nullable: false),
                    QueenCells = table.Column<bool>(nullable: false),
                    Pans = table.Column<bool>(nullable: false),
                    Liningcells = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Queen = table.Column<bool>(nullable: false),
                    Brood = table.Column<bool>(nullable: false),
                    Beepen = table.Column<bool>(nullable: false),
                    Varrora = table.Column<bool>(nullable: false),
                    PollenEntry = table.Column<bool>(nullable: false),
                    AirTraffic = table.Column<int>(nullable: false),
                    Temperature = table.Column<double>(nullable: false),
                    Weather = table.Column<string>(nullable: true),
                    MiteInfestation = table.Column<int>(nullable: false),
                    Weight = table.Column<double>(nullable: false),
                    Comment = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Observations", x => x.ObservationId);
                    table.ForeignKey(
                        name: "FK_Observations_AspNetUsers_CreatorId1",
                        column: x => x.CreatorId1,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Observations_AspNetUsers_DoneById",
                        column: x => x.DoneById,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Observations_Hives_HiveId1",
                        column: x => x.HiveId1,
                        principalTable: "Hives",
                        principalColumn: "HiveId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Processes",
                columns: table => new
                {
                    ProcessId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    CreatorId1 = table.Column<string>(nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    HiveId1 = table.Column<int>(nullable: true),
                    DueDateTime = table.Column<DateTime>(nullable: false),
                    DoneDateTime = table.Column<DateTime>(nullable: false),
                    DoneById = table.Column<string>(nullable: true),
                    IsDone = table.Column<bool>(nullable: false),
                    DronesFrames = table.Column<int>(nullable: false),
                    BrutFrames = table.Column<int>(nullable: false),
                    EmptyFrames = table.Column<int>(nullable: false),
                    LiningFrames = table.Column<int>(nullable: false),
                    MiddlewallFrame = table.Column<int>(nullable: false),
                    HoneyBox = table.Column<int>(nullable: false),
                    EmptyBox = table.Column<int>(nullable: false),
                    BeesEscape = table.Column<bool>(nullable: false),
                    Fence = table.Column<bool>(nullable: false),
                    Diaper = table.Column<bool>(nullable: false),
                    FlightholeWedge = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Comment = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Processes", x => x.ProcessId);
                    table.ForeignKey(
                        name: "FK_Processes_AspNetUsers_CreatorId1",
                        column: x => x.CreatorId1,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Processes_AspNetUsers_DoneById",
                        column: x => x.DoneById,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Processes_Hives_HiveId1",
                        column: x => x.HiveId1,
                        principalTable: "Hives",
                        principalColumn: "HiveId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Queens",
                columns: table => new
                {
                    QueenId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    CreatorId1 = table.Column<string>(nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    HiveId = table.Column<int>(nullable: false),
                    QueenBirthYear = table.Column<int>(nullable: false),
                    QueenPainted = table.Column<bool>(nullable: false),
                    QueenNo = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Queens", x => x.QueenId);
                    table.ForeignKey(
                        name: "FK_Queens_AspNetUsers_CreatorId1",
                        column: x => x.CreatorId1,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Queens_Hives_HiveId",
                        column: x => x.HiveId,
                        principalTable: "Hives",
                        principalColumn: "HiveId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Treatments",
                columns: table => new
                {
                    TreatmentId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    CreatorId1 = table.Column<string>(nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    HiveId1 = table.Column<int>(nullable: true),
                    DueDateTime = table.Column<DateTime>(nullable: false),
                    DoneDateTime = table.Column<DateTime>(nullable: false),
                    DoneById = table.Column<string>(nullable: true),
                    IsDone = table.Column<bool>(nullable: false),
                    TypeOfAgenttype = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Amount = table.Column<double>(nullable: false),
                    Brood = table.Column<bool>(nullable: false),
                    BeePen = table.Column<bool>(nullable: false),
                    Varroa = table.Column<bool>(nullable: false),
                    Weight = table.Column<double>(nullable: false),
                    Comment = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Treatments", x => x.TreatmentId);
                    table.ForeignKey(
                        name: "FK_Treatments_AspNetUsers_CreatorId1",
                        column: x => x.CreatorId1,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Treatments_AspNetUsers_DoneById",
                        column: x => x.DoneById,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Treatments_Hives_HiveId1",
                        column: x => x.HiveId1,
                        principalTable: "Hives",
                        principalColumn: "HiveId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Apiaries_CreatorId1",
                table: "Apiaries",
                column: "CreatorId1");

            migrationBuilder.CreateIndex(
                name: "IX_ApiaryApplicationUsers_ApplicationUserId",
                table: "ApiaryApplicationUsers",
                column: "ApplicationUserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_CreatorId1",
                table: "AspNetUsers",
                column: "CreatorId1");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Diaries_CreatorId1",
                table: "Diaries",
                column: "CreatorId1");

            migrationBuilder.CreateIndex(
                name: "IX_Feeds_CreatorId1",
                table: "Feeds",
                column: "CreatorId1");

            migrationBuilder.CreateIndex(
                name: "IX_Feeds_DoneById",
                table: "Feeds",
                column: "DoneById");

            migrationBuilder.CreateIndex(
                name: "IX_Feeds_HiveId1",
                table: "Feeds",
                column: "HiveId1");

            migrationBuilder.CreateIndex(
                name: "IX_HiveApplicationUsers_HiveId",
                table: "HiveApplicationUsers",
                column: "HiveId");

            migrationBuilder.CreateIndex(
                name: "IX_Hives_ApiaryId1",
                table: "Hives",
                column: "ApiaryId1");

            migrationBuilder.CreateIndex(
                name: "IX_Hives_CreatorId1",
                table: "Hives",
                column: "CreatorId1");

            migrationBuilder.CreateIndex(
                name: "IX_Hives_LocationId1",
                table: "Hives",
                column: "LocationId1");

            migrationBuilder.CreateIndex(
                name: "IX_HiveYieldAmounts_CreatorId1",
                table: "HiveYieldAmounts",
                column: "CreatorId1");

            migrationBuilder.CreateIndex(
                name: "IX_HiveYieldAmounts_DoneById",
                table: "HiveYieldAmounts",
                column: "DoneById");

            migrationBuilder.CreateIndex(
                name: "IX_HiveYieldAmounts_HiveId1",
                table: "HiveYieldAmounts",
                column: "HiveId1");

            migrationBuilder.CreateIndex(
                name: "IX_HiveYieldAmounts_YieldId1",
                table: "HiveYieldAmounts",
                column: "YieldId1");

            migrationBuilder.CreateIndex(
                name: "IX_LocationApplicationUsers_ApplicationUserId",
                table: "LocationApplicationUsers",
                column: "ApplicationUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Locations_ApiaryId1",
                table: "Locations",
                column: "ApiaryId1");

            migrationBuilder.CreateIndex(
                name: "IX_Locations_CreatorId1",
                table: "Locations",
                column: "CreatorId1");

            migrationBuilder.CreateIndex(
                name: "IX_Observations_CreatorId1",
                table: "Observations",
                column: "CreatorId1");

            migrationBuilder.CreateIndex(
                name: "IX_Observations_DoneById",
                table: "Observations",
                column: "DoneById");

            migrationBuilder.CreateIndex(
                name: "IX_Observations_HiveId1",
                table: "Observations",
                column: "HiveId1");

            migrationBuilder.CreateIndex(
                name: "IX_Processes_CreatorId1",
                table: "Processes",
                column: "CreatorId1");

            migrationBuilder.CreateIndex(
                name: "IX_Processes_DoneById",
                table: "Processes",
                column: "DoneById");

            migrationBuilder.CreateIndex(
                name: "IX_Processes_HiveId1",
                table: "Processes",
                column: "HiveId1");

            migrationBuilder.CreateIndex(
                name: "IX_Queens_CreatorId1",
                table: "Queens",
                column: "CreatorId1");

            migrationBuilder.CreateIndex(
                name: "IX_Queens_HiveId",
                table: "Queens",
                column: "HiveId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Treatments_CreatorId1",
                table: "Treatments",
                column: "CreatorId1");

            migrationBuilder.CreateIndex(
                name: "IX_Treatments_DoneById",
                table: "Treatments",
                column: "DoneById");

            migrationBuilder.CreateIndex(
                name: "IX_Treatments_HiveId1",
                table: "Treatments",
                column: "HiveId1");

            migrationBuilder.CreateIndex(
                name: "IX_Yields_CreatorId1",
                table: "Yields",
                column: "CreatorId1");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ApiaryApplicationUsers");

            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "Diaries");

            migrationBuilder.DropTable(
                name: "Feeds");

            migrationBuilder.DropTable(
                name: "HiveApplicationUsers");

            migrationBuilder.DropTable(
                name: "HiveYieldAmounts");

            migrationBuilder.DropTable(
                name: "LocationApplicationUsers");

            migrationBuilder.DropTable(
                name: "Observations");

            migrationBuilder.DropTable(
                name: "Processes");

            migrationBuilder.DropTable(
                name: "Queens");

            migrationBuilder.DropTable(
                name: "Treatments");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "Yields");

            migrationBuilder.DropTable(
                name: "Hives");

            migrationBuilder.DropTable(
                name: "Locations");

            migrationBuilder.DropTable(
                name: "Apiaries");

            migrationBuilder.DropTable(
                name: "AspNetUsers");
        }
    }
}
