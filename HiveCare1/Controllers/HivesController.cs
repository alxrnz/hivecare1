﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HiveCare1.Data;
using HiveCare1.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace HiveCare1.Controllers
{
    public class HivesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly ApplicationUserManager _userManager;

        public HivesController(ApplicationDbContext context, ApplicationUserManager userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: Hives
        public async Task<IActionResult> Index()
        {
            var user = await _userManager.GetCurrentUserAsync();
            var isSu = await _userManager.IsInRoleAsync(user, "Superuser");
            return View(await _context.Hives
                .Include(hive => hive.Queen)
                .Include(hive => hive.Location)
                .ThenInclude(x => x.Apiary)
                .Include(hive => hive.Location.Users)
                .ThenInclude(applicationUser => applicationUser.ApplicationUser)
                .Include(hive => hive.Users)
                .ThenInclude(applicationUser => applicationUser.ApplicationUser)
                .Where(x => x.Admins.Contains(user) || isSu || x.Location.Admins.Contains(user)).ToListAsync()
            );
        }

        // GET: Hives/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null) return NotFound();

            var hive = await _context.Hives
                .FirstOrDefaultAsync(m => m.HiveId == id);
            if (hive == null) return NotFound();

            return View(hive);
        }

        // GET: Hives/Create
        public async Task<IActionResult> Create(int? locationid)
        {
            if (locationid != null)
            {
                return View(new Hive()
                {
                    LocationId = locationid??0,
                    Location = await _context.Locations.FindAsync(locationid)
                });
            }
            return View();
        }

        // POST: Hives/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(
            [Bind(
                "HiveId,Name,Type,QueenBirthYear,QueenNo,QueenPainted,Gentleness,Loyalty,Inertia,Aggressiveness,LocationId")]
            Hive hive, int? locationid)
        {
            hive.Creator = await _userManager.GetCurrentUserAsync();
            hive.CreatorId = hive.Creator.Id;
            hive.CreationDate = DateTime.Now;

            hive.Location =
                await _context.Locations.FirstOrDefaultAsync(location => location.LocationId == hive.LocationId);
            hive.Location.Hives.Add(hive);
            if (ModelState.IsValid)
            {
                await SetUsers(hive);
                _context.Add(hive);
                
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Edit),new {id = hive.HiveId});
            }

            return View(hive);
        }

        private async Task<int> SetUsers(Hive modelToEdit)
        {

            var adminEmails = Request.Form["AdminUsers"].ToString().Split(',').Where(x => x != "").ToList();
            var beekeeperEmails = Request.Form["BeekeeperUsers"].ToString().Trim(',').Split(',').Where(x => x != "")
                .ToList();
            var dellist = modelToEdit.Users.Where(user =>
                !adminEmails.Contains(user.ApplicationUser.Email) ||
                !beekeeperEmails.Contains(user.ApplicationUser.Email)).ToList();

            foreach (var applicationUser in dellist)
                modelToEdit.Users.Remove(applicationUser);

            foreach (var adminEmail in adminEmails)
            {
                var admin = await _userManager.FindByEmailAsync(adminEmail);
                modelToEdit.Users.Add(new HiveApplicationUser()
                {
                    ApplicationUser = admin,
                    ApplicationUserId = admin.Id,
                    Hive = modelToEdit,
                    HiveId = modelToEdit.HiveId,
                    IsAdmin = true
                });
            }

            foreach (var email in beekeeperEmails)
            {
                var beekeeper = await _userManager.FindByEmailAsync(email);
                modelToEdit.Users.Add(new HiveApplicationUser()
                {
                    ApplicationUser = beekeeper,
                    ApplicationUserId = beekeeper.Id,
                    Hive = modelToEdit,
                    HiveId = modelToEdit.HiveId,
                    IsAdmin = false
                });
            }

            return 1;
        }

        // GET: Hives/Edit/5
            public async Task<IActionResult> Edit(int? id)
        {
            if (id == null) return NotFound();

            var hive = await _context.Hives
                .Include(hive1 => hive1.Users).ThenInclude(a => a.ApplicationUser)
                .Include(hive1 => hive1.Queen)
                .Include(hive1 => hive1.Location)
                .FirstOrDefaultAsync(hive1 => hive1.HiveId == id);
            if (hive == null) return NotFound();
            return View(hive);
        }

        // POST: Hives/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id,
            [Bind(
                "HiveId,Name,Type,Queen.QueenNo,Queen.QueenPainted,Gentleness,Loyalty,Inertia,Aggressiveness,CreationDate,QueenId")]
            Hive hive)
        {
            if (id != hive.HiveId) return NotFound();

            if (ModelState.IsValid)
            {
                //hive.Users = await _context.HiveApplicationUsers.Where(x => x.HiveId == id).ToListAsync();
               
                hive.Queen = await _context.Queens.FindAsync(hive.QueenId);
                try
                {
                    //var queen = await _context.Queens.FirstOrDefaultAsync(x => x.HiveId == hive.HiveId);
                    if (hive.Queen == null)
                    {
                        var user = await _userManager.GetCurrentUserAsync();
                        hive.Queen = new Queen
                        {
                            Hive = hive,
                            Creator = user,
                            CreationDate = DateTime.Now,
                            CreatorId = user.Id
                        };
                    }

                    hive.Queen.QueenBirthYear = Convert.ToInt32(Request.Form["QueenBirthYear"]);
                    hive.Queen.QueenNo = Convert.ToInt32(Request.Form["QueenNo"]);
                    hive.Queen.QueenPainted = Convert.ToBoolean(Request.Form["QueenPainted"]);
                    _context.Queens.Update(hive.Queen);
                    await SetUsers(hive);
                    await _context.SaveChangesAsync();
                    
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!HiveExists(hive.HiveId))
                        return NotFound();
                    throw;
                }

                return RedirectToAction(nameof(Edit),hive.HiveId);
            }

            return View(hive);
        }

        // GET: Hives/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null) return NotFound();

            var hive = await _context.Hives
                .FirstOrDefaultAsync(m => m.HiveId == id);
            if (hive == null) return NotFound();

            return View(hive);
        }

        // POST: Hives/Delete/5
        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var hive = await _context.Hives.FindAsync(id);
            _context.Hives.Remove(hive);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool HiveExists(int id)
        {
            return _context.Hives.Any(e => e.HiveId == id);
        }

        public async Task<IEnumerable<SelectListItem>> GetListAsync()
        {
            return await _context.Hives.Select(x => new SelectListItem(x.Name, x.HiveId.ToString())).ToListAsync();
        }

        public async Task<IEnumerable<Hive>> ListHivessAsync()
        {
            var user = await _userManager.GetCurrentUserAsync();
            var isSu = await _userManager.IsInRoleAsync(user, "Superuser");
            return await _context.Hives
                .Include(hive => hive.Users)
                .Include(hive => hive.Creator)
                .Include(hive => hive.Location)
                .ThenInclude(location => location.Apiary)
                .Where(hive => hive.Users.Any(admin => admin.ApplicationUserId == user.Id) ||hive.Location.Users.Any(admin => admin.ApplicationUserId == user.Id)||hive.Location.Apiary.Users.Any(admin => admin.ApplicationUserId == user.Id) ||
                                 hive.Creator.Id == user.Id || isSu)
                .ToListAsync();
            
        }
        public async Task<IEnumerable<Hive>> ListHivesForLocationAsync(int? locationid)
        {
            var user = await _userManager.GetCurrentUserAsync();
            var isSu = await _userManager.IsInRoleAsync(user, "Superuser");
            return await _context.Hives
                .Include(hive => hive.Users)
                .Include(hive => hive.Creator)
                .Include(hive => hive.Location)
                .ThenInclude(location => location.Apiary)
                .Where(hive => hive.Location.LocationId == locationid && hive.Users.Any(admin => admin.ApplicationUserId == user.Id) ||
                                 hive.Creator.Id == user.Id || isSu)
                .ToListAsync();
            
        }

        public async Task<IActionResult> RemoveQueen(int id)
        {
            var model = await _context.Hives.Include(hive => hive.Queen).FirstOrDefaultAsync(hive => hive.HiveId == id);
            if (model == null)
                return NotFound();
            _context.Queens.Remove(model.Queen);
            await _context.SaveChangesAsync();
            return RedirectToAction("Edit", new {id = model.HiveId});
        }

        public async Task<IActionResult> AddQueen(int id)
        {
            var queen = new Queen();
            var hive = await _context.Hives.FindAsync(id);
            hive.Queen = queen;
            _context.Update(hive);
            await _context.SaveChangesAsync();
            return RedirectToAction("Edit", new {id = hive.HiveId});
        }
    }
}