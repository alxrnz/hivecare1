﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HiveCare1.Data;
using HiveCare1.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace HiveCare1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BeekeepingApiController : ControllerBase
    {
        private readonly ApplicationUserManager _userManager;
        private readonly ApplicationDbContext _context;

        public BeekeepingApiController(ApplicationUserManager userManager, ApplicationDbContext context)
        {
            _userManager = userManager;
            _context = context;
        }
        [HttpPost("AddObservation")]
        [DisableRequestSizeLimit]
        public async Task<IActionResult> AddObservation([FromForm] Observation observation, int? hiveid)
        {

            var hive = await _context.Hives.FirstOrDefaultAsync(x => x.HiveId == observation.HiveId);
            if (hive == null) return NotFound();
            observation.Creator = await _userManager.GetCurrentUserAsync();
            observation.CreationDate = DateTime.Now;
            observation.Hive = hive;
            observation.HiveId = hiveid ?? 0;
            observation.Hive.Observations.Add(observation);
            _context.Add(observation);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index", "Beekeeping", new
            {
                apiaryid = observation.Hive.Location.Apiary.ApiaryId,
                locationid = observation.Hive.Location.LocationId,
                hiveid = observation.Hive.HiveId,
                activeslide = "feed",
            });
            
        }
        [HttpPost("EditObservation")]
        [DisableRequestSizeLimit]
        public async Task<IActionResult> EditObservation([FromForm] Observation observation)
        {
            var f = Request.Form;
            var obs = await _context.Observations.FindAsync(observation.ObservationId);
            obs.AirTraffic = observation.AirTraffic;
            obs.Liningcells = observation.Liningcells;
            obs.Beepen = observation.Beepen;
            obs.Brood = observation.Brood;
            obs.Comment = observation.Comment;
            obs.MiteInfestation = observation.MiteInfestation;
            obs.Pans = observation.Pans;
            obs.PollenEntry = observation.PollenEntry;
            obs.Queen = observation.Queen;
            obs.QueenCells = observation.QueenCells;
            obs.Temperature = observation.Temperature;
            obs.Varrora = observation.Varrora;
            obs.Weather = observation.Weather;
            obs.Weight = observation.Weight;
            obs.DoneDateTime = DateTime.Now;
            obs.DoneBy = await _userManager.GetCurrentUserAsync();
            obs.IsDone = true;
            _context.Update(obs);
            await _context.SaveChangesAsync();
            return Ok(obs);
        }

        [HttpPost("AddFeed")]
        [DisableRequestSizeLimit]
        public async Task<IActionResult> AddFeed([FromForm] Feed feed, int? hiveid)
        {

            var hive = await _context.Hives.FirstOrDefaultAsync(x => x.HiveId == feed.HiveId);
            if (hive == null) return NotFound();
            feed.Creator = await _userManager.GetCurrentUserAsync();
            feed.CreationDate = DateTime.Now;
            feed.Hive = hive;
            feed.HiveId = hiveid ?? 0;
            feed.Hive.Feeds.Add(feed);
            _context.Add(feed);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index", "Beekeeping", new
            {
                apiaryid = feed.Hive.Location.Apiary.ApiaryId,
                locationid = feed.Hive.Location.LocationId,
                hiveid = feed.Hive.HiveId,
                activeslide = "feed",
            });
            
        }
        [HttpPost("EditFeed")]
        [DisableRequestSizeLimit]
        public async Task<IActionResult> EditFeed([FromForm] Feed observation)
        {
            var feed = await _context.Feeds.FindAsync(observation.FeedId);
            feed.Amout = observation.Amout;
            feed.Comment = observation.Comment;
            feed.FeedType = observation.FeedType;
            feed.DoneDateTime = DateTime.Now;
            feed.DoneBy = await _userManager.GetCurrentUserAsync();
            feed.IsDone = true;
            _context.Update(feed);
            await _context.SaveChangesAsync();
            return Ok(feed);
        }

        [HttpPost("AddProcess")]
        [DisableRequestSizeLimit]
        public async Task<IActionResult> AddProcess([FromForm] Process process, int? hiveid)
        {
            var hive = await _context.Hives.FirstOrDefaultAsync(x => x.HiveId == process.HiveId);
            if (hive == null) return NotFound();
            process.Creator = await _userManager.GetCurrentUserAsync();
            process.CreationDate = DateTime.Now;
            process.Hive = hive;
            process.HiveId = process.HiveId;
            process.Hive.Processes.Add(process);
            _context.Add(process);
            await _context.SaveChangesAsync();

            return RedirectToAction("Index", "Beekeeping", new
            {
                apiaryid = process.Hive.Location.Apiary.ApiaryId,
                locationid = process.Hive.Location.LocationId,
                hiveid = process.Hive.HiveId,
                activeslide = "process",
            });
        }

        [HttpPost("EditProcess")]
        [DisableRequestSizeLimit]
        public async Task<IActionResult> EditProcess([FromForm] Process process)
        {
            var process_ = await _context.Processes.FindAsync(process.ProcessId);
            process_.BeesEscape = process.BeesEscape;
            process_.BrutFrames = process.BrutFrames;
            process_.Diaper = process.Diaper;
            process_.Comment = process.Comment;
            process_.DronesFrames = process.DronesFrames;
            process_.EmptyBox = process.EmptyBox;
            process_.EmptyFrames = process.EmptyFrames;
            process_.Fence = process.Fence;
            process_.FlightholeWedge = process.FlightholeWedge;
            process_.HoneyBox = process.HoneyBox;
            process_.DoneDateTime = DateTime.Now;
            process_.DoneBy = await _userManager.GetCurrentUserAsync();
            process_.IsDone = true;
            _context.Update(process_);
            await _context.SaveChangesAsync();
            return Ok(process_);
        }
        [HttpPost("AddTreatment")]
        [DisableRequestSizeLimit]
        public async Task<IActionResult> AddTreatment([FromForm] Treatment treatment, int? hiveid)
        {
            var hive = await _context.Hives.FirstOrDefaultAsync(x => x.HiveId == treatment.HiveId);
            if (hive == null) return NotFound();
            treatment.Creator = await _userManager.GetCurrentUserAsync();
            treatment.CreationDate = DateTime.Now;
            treatment.Hive = hive;
            treatment.HiveId = treatment.HiveId;
            treatment.Hive.Treatments.Add(treatment);
            _context.Add(treatment);
            await _context.SaveChangesAsync();

            return RedirectToAction("Index", "Beekeeping", new
            {
                apiaryid = treatment.Hive.Location.Apiary.ApiaryId,
                locationid = treatment.Hive.Location.LocationId,
                hiveid = treatment.Hive.HiveId,
                activeslide = "process",
            });
        }

        [HttpPost("EditTreatment/{id}")]
        [DisableRequestSizeLimit]
        public async Task<IActionResult> EditTreatment([FromForm] Treatment treatment)
        {
            var treatment_ = await _context.Treatments.FindAsync(treatment.TreatmentId);
            treatment_.Amount = treatment.Amount;
            treatment_.BeePen = treatment.BeePen;
            treatment_.Brood = treatment.Brood;
            treatment_.Comment = treatment.Comment;
            treatment_.TypeOfAgenttype = treatment.TypeOfAgenttype;
            treatment_.Varroa = treatment.Varroa;
            treatment_.Weight = treatment.Weight;
            treatment_.DoneDateTime = DateTime.Now;
            treatment_.DoneBy = await _userManager.GetCurrentUserAsync();
            treatment_.IsDone = true;
            _context.Update(treatment_);
            await _context.SaveChangesAsync();
            return Ok(treatment_);
        }

        [HttpPost("AddHiveYield")]
        [DisableRequestSizeLimit]
        public async Task<IActionResult> AddHiveYield([FromForm] HiveYieldAmount yieldAmount, int? hiveid)
        {
            var hive = await _context.Hives.FirstOrDefaultAsync(x => x.HiveId == yieldAmount.HiveId);
            if (hive == null) return NotFound();
            yieldAmount.Creator = await _userManager.GetCurrentUserAsync();
            yieldAmount.CreationDate = DateTime.Now;
            yieldAmount.Hive = hive;
            yieldAmount.HiveId = yieldAmount.HiveId;
            yieldAmount.Hive.YieldAmounts.Add(yieldAmount);
            _context.Add(yieldAmount);
            await _context.SaveChangesAsync();

            return RedirectToAction("Index", "Beekeeping", new
            {
                apiaryid = yieldAmount.Hive.Location.Apiary.ApiaryId,
                locationid = yieldAmount.Hive.Location.LocationId,
                hiveid = yieldAmount.Hive.HiveId,
                activeslide = "@yield",
            });
        }

        [HttpPost("EditHiveYield/{id}")]
        [DisableRequestSizeLimit]
        public async Task<IActionResult> EditHiveYield([FromForm] HiveYieldAmount yieldAmount)
        {
            var hiveYieldAmount = await _context.HiveYieldAmounts.FindAsync(yieldAmount.HiveYieldAmountId);
            hiveYieldAmount.Amount = yieldAmount.Amount;
            hiveYieldAmount.HoneycombCount = yieldAmount.HoneycombCount;
            hiveYieldAmount.WaterShare = yieldAmount.WaterShare;
            hiveYieldAmount.DoneDateTime = DateTime.Now;
            hiveYieldAmount.DoneBy = await _userManager.GetCurrentUserAsync();
            hiveYieldAmount.IsDone = true;
            _context.Update(hiveYieldAmount);
            await _context.SaveChangesAsync();
            return Ok(hiveYieldAmount);
        }
        [HttpPost("AddYield")]
        [DisableRequestSizeLimit]
        public async Task<IActionResult> AddYield([FromForm] Yield yield, int? hiveid)
        {
            yield.Creator = await _userManager.GetCurrentUserAsync();
            yield.CreationDate = DateTime.Now;
            yield.Amounts = new List<HiveYieldAmount>();
            var items = Request.Form["hiveyields"].ToString().Split(";");
            foreach (var hiveYieldAmountid in items)
            {
                var hiveYieldAmount = await _context.HiveYieldAmounts.FirstOrDefaultAsync(x =>
                    x.HiveYieldAmountId == Convert.ToInt32(hiveYieldAmountid));
                yield.Amounts.Add(new HiveYieldAmount()
                {
                  Hive  = hiveYieldAmount.Hive,
                  HiveId = hiveYieldAmount.HiveId,
                  Yield = yield,
                  YieldId = yield.YieldId,
                });
            }
            _context.Add(yield);
            await _context.SaveChangesAsync();

            return RedirectToAction("Index", "Admin");
        }
    }
    
}