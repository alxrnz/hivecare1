﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HiveCare1.Data;
using HiveCare1.Models;
using HiveCare1.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace HiveCare1.Controllers
{
    [Authorize]
    public class LocationsController : Controller
    {

        private readonly ApplicationDbContext _context;
        private readonly ApplicationUserManager _userManager;
        public LocationsController(ApplicationDbContext context, ApplicationUserManager userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: Locations
        public async Task<IActionResult> Index()
        {
            return View(await _context.Locations.ToListAsync());
        }
        
        public async Task<IActionResult> Create(int? apiaryid)
        {
            var user = await _userManager.GetCurrentUserAsync();
            if (user.Apiaries.Count > 0 && apiaryid != null)
            {
                return View(new Location()
                {
                    ApiaryId = apiaryid??0,
                    Apiary = await _context.Apiaries.FindAsync(apiaryid),
                });
            }
            else
            {
                return View(new Location());
            }

            return RedirectToAction(nameof(Index));
        }

        // POST: Locations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(
            [Bind("LocationId,Name,Street,HouseNo,Zip,City,Latitude,Longitude,ApiaryId")]
            Location location)
        {
            if (ModelState.IsValid)
            {
                    location.Creator = await _userManager.GetCurrentUserAsync();
                    location.CreatorId = location.Creator.Id;
                    location.CreationDate = DateTime.Now;
                    location.Users.Add(
                        new LocationApplicationUser
                        {
                            LocationId = location.ApiaryId,
                            ApplicationUserId = location.Creator.Id
                        });
                    var apiary = await _context.Apiaries.FirstOrDefaultAsync(x => x.ApiaryId == location.ApiaryId);
                    location.Apiary = apiary;
                    location.Apiary.Locations.Add(location);
                    location.ApiaryId = apiary.ApiaryId;
                    location.Users = new List<LocationApplicationUser>();
                    await SetUsers(location);
                    _context.Add(location);
                    await _context.SaveChangesAsync();
               

                return RedirectToAction(nameof(Index));
            }

            return View(location);
        }

        // GET: Locations/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null) return NotFound();

            var location = await _context.Locations
                .FirstOrDefaultAsync(m => m.LocationId == id);
            if (location == null) return NotFound();

            return View(location);
        }

        // GET: Locations/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null) return NotFound();

            var location = await _context.Locations.Include(locati => locati.Creator).Include(loc => loc.Users)
                .Include(l => l.Apiary).Include(loc => loc.Hives).ThenInclude(hive => hive.Queen).FirstOrDefaultAsync(x => x.LocationId == id);
            if (location == null) return NotFound();
            location.ApiaryId = location.Apiary.ApiaryId;
            return View(location);
        }

        // POST: Locations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id,
            [Bind("Name,Street,HouseNo,Zip,City,LocationId,Latitude,Longitude,CreatorId,ApiaryId")]
            Location location)
        {
            var test = Request.Form["ApiaryId"];
            if (id != location.LocationId) return NotFound();
            var dblocation = await _context.Locations.FindAsync(location.LocationId);

            var name = location.Name;
            var locationId = location.LocationId;
            var street = location.Street;
            var houseno = location.HouseNo;
            var zip = location.Zip;
            var city = location.City;
            var lat = location.Latitude;
            var lon = location.Longitude;
            

            if (ModelState.IsValid)
            {
                 var modelToEdit = await _context.Locations
                .Include(location1 => location1.Apiary)
                .Include(location1 => location1.Users)
                .ThenInclude(locationApplicationUser => locationApplicationUser.ApplicationUser)
                .Where(location1 => location1.LocationId == id)
                .FirstOrDefaultAsync();

                 if (modelToEdit.Name != name) modelToEdit.Name = name;
                 if (modelToEdit.Street != street) modelToEdit.Street = street;
                 if (modelToEdit.HouseNo != houseno) modelToEdit.HouseNo = houseno;
                 if (modelToEdit.Zip != zip) modelToEdit.Zip = zip;
                 if (modelToEdit.City != city) modelToEdit.City = city;
                 if (modelToEdit.Latitude != lat) modelToEdit.Latitude = lat;
                 if (modelToEdit.Longitude != lon) modelToEdit.Longitude = lon;

                await SetUsers(modelToEdit);

                try
                {
                    _context.Update(modelToEdit);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!LocationExists(modelToEdit.LocationId))
                        return NotFound();
                    throw;
                }

                return RedirectToAction(nameof(Edit),new {id = modelToEdit.LocationId});
            }

            return View(location);
        }

        private async Task<int> SetUsers(Location modelToEdit)
        {

            var adminEmails = Request.Form["AdminUsers"].ToString().Split(',').Where(x => x != "").ToList();
            var beekeeperEmails = Request.Form["BeekeeperUsers"].ToString().Trim(',').Split(',').Where(x => x != "")
                .ToList();
            var dellist = modelToEdit.Users.Where(user =>
                {
                    var usertest = _context.ApplicationUsers.Find(user.ApplicationUserId);
                    return !adminEmails.Contains(user.ApplicationUser.Email) ||
                           !beekeeperEmails.Contains(user.ApplicationUser.Email);
                }
                ).ToList();

            foreach (var applicationUser in dellist)
                modelToEdit.Users.Remove(applicationUser);

            foreach (var adminEmail in adminEmails)
            {
                var admin = await _userManager.FindByEmailAsync(adminEmail);
                modelToEdit.Users.Add(new LocationApplicationUser()
                {
                    ApplicationUser = admin,
                    ApplicationUserId = admin.Id,
                    Location = modelToEdit,
                    LocationId = modelToEdit.LocationId,
                    IsAdmin = true
                });
            }

            foreach (var email in beekeeperEmails)
            {
                var beekeeper = await _userManager.FindByEmailAsync(email);
                modelToEdit.Users.Add(new LocationApplicationUser()
                {
                    ApplicationUser = beekeeper,
                    ApplicationUserId = beekeeper.Id,
                    Location = modelToEdit,
                    LocationId = modelToEdit.LocationId,
                    IsAdmin = false
                });
            }

            return 1;
        }

        // GET: Locations/Delete/5
            public async Task<IActionResult> Delete(int? id)
        {
            if (id == null) return NotFound();

            var location = await _context.Locations
                .FirstOrDefaultAsync(m => m.LocationId == id);
            if (location == null) return NotFound();

            return View(location);
        }

        // POST: Locations/Delete/5
        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var location = await _context.Locations.FindAsync(id);
            _context.Locations.Remove(location);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool LocationExists(int id)
        {
            return _context.Locations.Any(e => e.LocationId == id);
        }

        public async Task<IList<Location>> ListLocationsAsync()
        {
            var user = await _userManager.GetCurrentUserAsync();
            var apiarieLocations = (await _context.ApiaryApplicationUsers
                    .Include(a => a.Apiary)
                    .Where(x => x.ApplicationUserId == user.Id)
                    .ToListAsync())
                .SelectMany(x => x.Apiary.Locations)
                .ToList();

            var directLocations =
                (await _context.Locations.Include(x => x.Users).ToListAsync()).Where(x =>
                    x.Admins.Contains(user)).ToList();
            apiarieLocations.AddRange(directLocations);
            return new List<Location>(apiarieLocations);
            
        }

        public async Task<IEnumerable<SelectListItem>> GetListAsync()
        {
            var user = await _userManager.GetCurrentUserAsync();
            var locationEntrustedByApiaries =
                user.Apiaries.Select(api => api.Apiary).SelectMany(x => x.Locations).ToList();
            var directlyEntrustetLocations = user.Locations.Select(loc => loc.Location).Where(x => !locationEntrustedByApiaries.Contains(x)).ToList();
            locationEntrustedByApiaries.AddRange(directlyEntrustetLocations);
            return locationEntrustedByApiaries.Select(x => new SelectListItem(x.Name, x.LocationId.ToString()))
                .Distinct();
        }
    }
}