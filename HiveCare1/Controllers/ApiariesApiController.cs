﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HiveCare1.Data;
using HiveCare1.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace HiveCare1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ApiariesApiController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly ApplicationUserManager _userManager;

        public ApiariesApiController(ApplicationDbContext context, ApplicationUserManager userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: api/ApiariesApi
        [HttpGet]
        public IEnumerable<Apiary> GetApiary()
        {
            return _context.Apiaries;
        }

        // GET: api/ApiariesApi/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetApiary([FromRoute] int id)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var apiary = await _context.Apiaries.FindAsync(id);

            if (apiary == null) return NotFound();

            return Ok(apiary);
        }

        // PUT: api/ApiariesApi/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutApiary([FromRoute] int id, [FromBody] Apiary apiary)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            if (id != apiary.ApiaryId) return BadRequest();

            _context.Entry(apiary).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ApiaryExists(id))
                    return NotFound();
                throw;
            }

            return NoContent();
        }

        // POST: api/ApiariesApi/GetApiaryList
        [HttpGet(nameof(GetUserApiariesAsync))]
        public async Task<ApiaryApiModel> GetUserApiariesAsync()
        {
            var user = await _userManager.Users.Include(x => x.Apiaries).ThenInclude(x => x.Apiary)
                .FirstOrDefaultAsync(u => u.UserName == User.Identity.Name);
            var apis = await _context.Apiaries.Include(x => x.Users).ThenInclude(ad => ad.ApplicationUser)
                .Include(a => a.Locations).ThenInclude(a => a.Hives)
                .Where(x => x.Users.Any(u => u.ApplicationUserId == user.Id) || x.Creator.Id == user.Id)
                .ToListAsync();
            return new ApiaryApiModel(apis);
        }

        // POST: api/ApiariesApi
        [HttpPost]
        public async Task<IActionResult> PostApiary([FromBody] Apiary apiary)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            _context.Apiaries.Add(apiary);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetApiary", new {id = apiary.ApiaryId}, apiary);
        }

        // DELETE: api/ApiariesApi/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteApiary([FromRoute] int id)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var apiary = await _context.Apiaries.FindAsync(id);
            if (apiary == null) return NotFound();

            _context.Apiaries.Remove(apiary);
            await _context.SaveChangesAsync();

            return Ok(apiary);
        }

        private bool ApiaryExists(int id)
        {
            return _context.Apiaries.Any(e => e.ApiaryId == id);
        }
    }

    public class ApiaryApiModel
    {
        public List<Apiary> Apiaries { get; set; }

        public double Yields;
        public ApiaryApiModel(List<Apiary> apiaries)
        {
            Apiaries = apiaries;
            Yields = Apiaries.Sum(
                apiary => apiary.Locations.Sum(
                    locaction => locaction.Hives.Sum(
                        hive => hive.YieldAmounts.Sum(yield => yield.Yield.Amount)
                        )
                    )
                );
        }
    }
}