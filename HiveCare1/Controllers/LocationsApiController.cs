﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HiveCare1.Data;
using HiveCare1.Models;
using HiveCare1.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace HiveCare1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LocationsApiController : ControllerBase
    {
        private readonly ApiariesApiController _apiariesController;
        private readonly ApplicationDbContext _context;
        private readonly ApplicationUserManager _userManager;

        public LocationsApiController(ApplicationDbContext context, ApiariesApiController apiariesController,
            ApplicationUserManager usermanager)
        {
            _context = context;
            _apiariesController = apiariesController;
            _userManager = usermanager;

        }

        // GET: api/LocationsApi
        [HttpGet]
        public IEnumerable<Location> GetLocations()
        {
            return _context.Locations;
        }

        // GET: api/LocationsApi/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetLocation([FromRoute] int id)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var location = await _context.Locations.Include(a => a.Apiary).Include(x => x.Users)
                .ThenInclude(a => a.ApplicationUser).FirstOrDefaultAsync(x => x.LocationId == id);

            if (location == null) return NotFound();

            return Ok(location);
        }

        // GET: api/LocationsApi/5
        [HttpGet(nameof(GetUserLocationsAsync))]
        public async Task<List<Location>> GetUserLocationsAsync()
        {
            var user = await _userManager.Users.Include(x => x.Locations).ThenInclude(x => x.Location)
                .FirstOrDefaultAsync(u => u.UserName == User.Identity.Name);
            var locations = _context.Locations.Include(x => x.Users)
                .Where(x => x.Users.Any(u => u.ApplicationUserId == user.Id)).ToListAsync();
            return await _context.Locations.Include(x => x.Users)
                .Where(x => x.Users.Any(u => u.ApplicationUserId == user.Id)).ToListAsync();
        }

        // GET: api/LocationsApi/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetLocationsForApiary([FromRoute] int id)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var location = await _context.Locations.FindAsync(id);

            if (location == null) return NotFound();

            return Ok(location);
        }

        // PUT: api/LocationsApi/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutLocation([FromRoute] int id, [FromBody] Location location)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            if (id != location.LocationId) return BadRequest();

            _context.Entry(location).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LocationExists(id))
                    return NotFound();
                throw;
            }

            return NoContent();
        }

        // POST: api/LocationsApi
        [HttpPost]
        [DisableRequestSizeLimit]
        public async Task<IActionResult> PostLocation([FromForm] Location location)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            location.Creator = await _userManager.GetCurrentUserAsync();
            location.CreatorId = location.Creator.Id;
            location.CreationDate = DateTime.Now;
            var userIds = Request.Form["Users"].ToString().Split(';');
            foreach (var userId in userIds)
            {
                if (userId == "")
                    continue;
                var user = await _userManager.FindByEmailAsync(userId);
                location.Users.Add(
                    new LocationApplicationUser
                    {
                        Location = location,
                        LocationId = location.LocationId,
                        ApplicationUser = user,
                        ApplicationUserId = user.Id
                    });
            }

            await SetUsers(location);
            if (!User.IsInRole("Superuser"))
            {
                var suser = await _userManager.GetUserAsync(User);
                location.Users.Add(new LocationApplicationUser
                {
                    ApplicationUser = suser,
                    Location = location,
                    IsAdmin = true,
                });
            }
            
            var apiary = _context.Apiaries.Find(location.ApiaryId);
            location.Apiary = apiary;
            apiary.Locations.Add(location);
           
            _context.Update(apiary);
            _context.Locations.Add(location);
            await _context.SaveChangesAsync();
            return CreatedAtAction(nameof(GetLocation), new {id = location.LocationId}, location);
        }

        // DELETE: api/LocationsApi/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteLocation([FromRoute] int id)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var location = await _context.Locations.FindAsync(id);
            if (location == null) return NotFound();

            _context.Locations.Remove(location);
            await _context.SaveChangesAsync();

            return Ok(location);
        }

        private bool LocationExists(int id)
        {
            return _context.Locations.Any(e => e.LocationId == id);
        }
        private async Task<int> SetUsers(Location modelToEdit)
        {
            var removeAlist = Request.Form["RemoveAdmin"].ToString().Trim(',').Split(',').ToList();
            removeAlist.AddRange(modelToEdit.Admins.Select(x => x.Email));
            var adminEmails = Request.Form["AdminUsers"].ToString().Split(',').ToList();
            var beekeeperEmails = Request.Form["BeekeeperUsers"].ToString().Trim(',').Split(',').ToList();

            foreach (var user in removeAlist)
            {
                if (user == "") continue;
                adminEmails.Remove(user);
                beekeeperEmails.Remove(user);
                var userentity = await _userManager.FindByEmailAsync(user);
                var apiaryApplicationUser = await
                    _context.LocationApplicationUsers.FindAsync(modelToEdit.LocationId, userentity.Id);
                modelToEdit.Users.Remove(apiaryApplicationUser);
            }

            foreach (var admin in adminEmails)
            {
                if (admin == "" || modelToEdit.Admins.Any(x => x.Email == admin) ||
                    modelToEdit.Beekeepers.Any(x => x.Email == admin)) continue;
                var userentity = await _userManager.FindByEmailAsync(admin);
                var apiaryApplicationUser = new LocationApplicationUser()
                {
                    Location = modelToEdit,
                    LocationId = modelToEdit.LocationId,
                    ApplicationUser = userentity,
                    ApplicationUserId = userentity.Id,
                    IsAdmin = true
                };
                modelToEdit.Users.Add(apiaryApplicationUser);
            }

            foreach (var beekeeper in beekeeperEmails)
            {
                if (beekeeper == "" || modelToEdit.Beekeepers.Any(x => x.Email == beekeeper) ||
                    modelToEdit.Admins.Any(x => x.Email == beekeeper)) continue;
                var userentity = await _userManager.FindByEmailAsync(beekeeper);
                var apiaryApplicationUser = new LocationApplicationUser
                {
                    Location = modelToEdit,
                    LocationId = modelToEdit.LocationId,
                    ApplicationUser = userentity,
                    ApplicationUserId = userentity.Id,
                    IsAdmin = false
                };
                modelToEdit.Users.Add(apiaryApplicationUser);
            }

            return 1;
        }
    }

}