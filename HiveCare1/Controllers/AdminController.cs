﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HiveCare1.Data;
using HiveCare1.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace HiveCare1.Controllers
{
    [Authorize]
    public class AdminController : Controller
    {
        private readonly ApiariesController _apiariesController;
        private readonly ApplicationDbContext _context;
        private readonly HivesController _hivesController;
        private readonly LocationsController _locationsController;


        private readonly ApplicationUserManager _userManager;

        public AdminController(
            ApplicationDbContext context,
            ApplicationUserManager userManager,
            LocationsController locationsController,
            HivesController hivesController,
            ApiariesController apiariesController)
        {
            _userManager = userManager;
            _locationsController = locationsController;
            _hivesController = hivesController;
            _apiariesController = apiariesController;
            _context = context;
        }

        public async Task<IActionResult> Index(int? pageNumber)
        {
            var dashboardModel = await GetDashboardModel();

            return View(dashboardModel);
        }

        public async Task<IActionResult> Map(int? pageNumber)
        {
            var dashboardModel = await GetDashboardModel();

            return View(dashboardModel);
        }

        public async Task<IActionResult> Beekeeping(int? id)
        {
            var user = await _userManager.GetUserAsync(User);
            if (id == null)
                return NotFound();
            var hive = await _context.Hives.FindAsync(id);
            if (hive == null)
                return NotFound();
            return View(hive);
        }

        public IActionResult EditUser(int? id)
        {
            return RedirectToRoute("Identity/AdminPage/Edit", id);
        }

        private async Task<DashboardModel> GetDashboardModel()
        {
            var user = await _userManager.GetCurrentUserAsync();
            var dashboardModel = new DashboardModel();
            return dashboardModel;
        }

        public async Task<IActionResult> CreateYield()
        {
            return View();
        }

        [HttpPost("CreateYield")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateYield([FromForm] Yield yield)
        {
            if (ModelState.IsValid)
            {
                yield.Creator = await _userManager.GetCurrentUserAsync();
                yield.CreatorId = yield.Creator.Id;
                yield.CreationDate = DateTime.Now;
                yield.Amounts = new List<HiveYieldAmount>();
                var items = Request.Form["HiveYields"];
                foreach (var hiveYieldAmountid in items)
                {
                    var hiveYieldAmount = await _context.HiveYieldAmounts.FirstOrDefaultAsync(x =>
                        x.HiveYieldAmountId == Convert.ToInt32(hiveYieldAmountid));
                    yield.Amounts.Add(new HiveYieldAmount
                    {
                        Hive = hiveYieldAmount.Hive,
                        HiveId = hiveYieldAmount.HiveId,
                        Yield = yield,
                        YieldId = yield.YieldId
                    });
                }

                _context.Add(yield);
                await _context.SaveChangesAsync();

                return RedirectToAction("Index", "Admin");
            }

            return View(yield);
        }
    }

    public class DashboardModel
    {
        public IList<ApplicationUser> Users { get; set; }
        public IList<Location> Locations { get; set; }
        public IList<Hive> Hives { get; set; }
        public ICollection<Apiary> Apiaries { get; set; }
    }
}