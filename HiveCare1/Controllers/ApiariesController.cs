﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using HiveCare1.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SQLitePCL;

namespace HiveCare1.Models
{
    public class ApiariesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly ApplicationUserManager _userManager;
        private readonly ClaimsPrincipal _caller;

        public ApiariesController(ApplicationDbContext context, ApplicationUserManager userManager,
            IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _userManager = userManager;
            _caller = httpContextAccessor.HttpContext.User;
        }

        // GET: Apiaries
        public async Task<IActionResult> Index()
        {
            var user = await _userManager.GetCurrentUserAsync();
            var isSu = await _userManager.IsInRoleAsync(user, "Superuser");
            return View(await _context.Apiaries
                .Include(apiary => apiary.Locations)
                .ThenInclude(x => x.Hives)
                .ThenInclude(x => x.YieldAmounts)
                .ThenInclude(x => x.Yield)
                .Include(apiary => apiary.Locations)
                .ThenInclude(location => location.Users)
                .ThenInclude(applicationUser => applicationUser.ApplicationUser)
                .Include(apiary => apiary.Users)
                .ThenInclude(applicationUser => applicationUser.ApplicationUser)
                .Where(x => x.Admins.Contains(user) || isSu || x.Locations.SelectMany(y => y.Admins).Contains(user))
                .ToListAsync()
            );
        }

        // GET: Apiaries/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null) return NotFound();

            var apiary = await _context.Apiaries
                .FirstOrDefaultAsync(m => m.ApiaryId == id);
            if (apiary == null) return NotFound();

            return View(apiary);
        }

        // GET: Apiaries/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Apiaries/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ApiaryId,Name")] Apiary apiary)
        {
            if (ModelState.IsValid)
            {

                apiary.Creator = await _userManager.GetCurrentUserAsync();
                apiary.CreatorId = apiary.Creator.Id;
                apiary.CreationDate = DateTime.Now;
                apiary.Users = new List<ApiaryApplicationUser>();
                await SetUsers(apiary);
                _context.Add(apiary);

                await _context.SaveChangesAsync();

                return RedirectToAction(nameof(Edit), new {id = apiary.ApiaryId});
            }

            return View(apiary);
        }

        // GET: Apiaries/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null) return NotFound();
            
            var apiary = await _context.Apiaries
                .Include(a => a.Creator)
                .Include(a => a.Locations)
                .ThenInclude(location => location.Hives)
                .Include(b => b.Users)
                .ThenInclude(x => x.ApplicationUser).SingleOrDefaultAsync(x => x.ApiaryId == id);
            if (apiary == null) return NotFound();
            return View(apiary);
        }

        // POST: Apiaries/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ApiaryId,Name")] Apiary apiary)
        {
            if (id != apiary.ApiaryId) return NotFound();
            
            var apiaryName = apiary.Name;
            var apiaryId = apiary.ApiaryId;

            if (ModelState.IsValid)
            {
                var modelToEdit = await _context.Apiaries
                    .Include(ap => ap.Users)
                    .ThenInclude(x => x.ApplicationUser)
                    .Where(api => api.ApiaryId == apiary.ApiaryId)
                    .FirstOrDefaultAsync();
                
                if (modelToEdit == null) return NotFound();

                await SetUsers(modelToEdit);


                if (apiaryName != modelToEdit.Name)
                {
                    modelToEdit.Name = apiary.Name;
                }

                var result = await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Edit), new { id = apiary.ApiaryId });
            }
            return View(apiary);
        }

        private async Task<int> SetUsers(Apiary modelToEdit)
        {
            
            var adminEmails = Request.Form["AdminUsers"].ToString().Split(',').Where(x => x != "").ToList();
            var beekeeperEmails = Request.Form["BeekeeperUsers"].ToString().Trim(',').Split(',').Where(x => x != "").ToList();
            var dellist = modelToEdit.Users.Where( user =>
                !adminEmails.Contains(user.ApplicationUser.Email) ||
                !beekeeperEmails.Contains(user.ApplicationUser.Email)).ToList();

            foreach (var applicationUser in dellist)
                modelToEdit.Users.Remove(applicationUser);

            foreach (var adminEmail in adminEmails)
            {
                var admin = await _userManager.FindByEmailAsync(adminEmail);
                modelToEdit.Users.Add(new ApiaryApplicationUser()
                {
                    ApplicationUser = admin,
                    ApplicationUserId = admin.Id,
                    Apiary = modelToEdit,
                    ApiaryId = modelToEdit.ApiaryId,
                    IsAdmin = true
                });
            }
            foreach (var email in beekeeperEmails)
            {
                var beekeeper = await _userManager.FindByEmailAsync(email);
                modelToEdit.Users.Add(new ApiaryApplicationUser()
                {
                    ApplicationUser = beekeeper,
                    ApplicationUserId = beekeeper.Id,
                    Apiary = modelToEdit,
                    ApiaryId = modelToEdit.ApiaryId,
                    IsAdmin = false
                });
            }
            var users = modelToEdit.Users;
            var breakfix = 0;
            return 0;
        }

        // GET: Apiaries/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null) return NotFound();

            var apiary = await _context.Apiaries
                .FirstOrDefaultAsync(m => m.ApiaryId == id);
            if (apiary == null) return NotFound();

            return View(apiary);
        }

        // POST: Apiaries/Delete/5
        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var apiary = await _context.Apiaries.FindAsync(id);
            _context.Apiaries.Remove(apiary);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ApiaryExists(int id)
        {
            return _context.Apiaries.Any(e => e.ApiaryId == id);
        }

        public async Task<IEnumerable<SelectListItem>> GetListAsync()
        {
            var user = await _userManager.GetCurrentUserAsync();
            return user.Apiaries.Select(x => x.Apiary).Select(x => new SelectListItem(x.Name, x.ApiaryId + ""));
        }

        public async Task<ICollection<Apiary>> ListApiariesAsync()
        {
            var user = await _userManager.GetCurrentUserAsync();
            var isSu = await _userManager.IsInRoleAsync(user, "Superuser");
            return await _context.Apiaries
                .Include(apiary => apiary.Users)
                .Include(apiary => apiary.Creator)
                .Include(apiary => apiary.Locations)
                .ThenInclude(location => location.Hives)
                .Where(apiary => apiary.Users.Any(admin => admin.ApplicationUserId == user.Id) ||
                                 apiary.Creator.Id == user.Id || isSu )
                .ToListAsync();
        }
    }
}
