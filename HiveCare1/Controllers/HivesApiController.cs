﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HiveCare1.Data;
using HiveCare1.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace HiveCare1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HivesApiController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly ApplicationUserManager _userManager;

        public HivesApiController(ApplicationDbContext context, ApplicationUserManager userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: api/HivesApi
        [HttpGet]
        public JsonResult GetHive()
        {
            return new JsonResult(_context.Hives.ToList());
        }

        // GET: api/HivesApi/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetHive([FromRoute] int id)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var hive = await _context.Hives.FindAsync(id);

            if (hive == null) return NotFound();

            return Ok(hive);
        }

        // PUT: api/HivesApi/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutHive([FromRoute] int id, [FromBody] Hive hive)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            if (id != hive.HiveId) return BadRequest();

            _context.Entry(hive).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!HiveExists(id))
                    return NotFound();
                throw;
            }

            return NoContent();
        }

        // POST: api/HivesApi/GetHiveList
        [HttpGet(nameof(GetUserHivesAsync))]
        public async Task<List<Hive>> GetUserHivesAsync()
        {
            var user = await _userManager.Users.Include(x => x.Hives).ThenInclude(x => x.Hive)
                .FirstOrDefaultAsync(u => u.UserName == User.Identity.Name);
            return await _context.Hives.Include(x => x.Users).ThenInclude(admin => admin.ApplicationUser)
                .Where(x => x.Users.Any(u => u.ApplicationUserId == user.Id) ||
                            x.Creator.UserName == user.UserName).ToListAsync();
        }

        // POST: api/HivesApi
        [HttpPost]
        [DisableRequestSizeLimit]
        public async Task<IActionResult> PostHive([FromForm] Hive hive)
        {
            hive.Creator = await _userManager.GetUserAsync(User);
            hive.CreatorId = hive.Creator.Id;
            hive.CreationDate = DateTime.Now;
            var userIds = Request.Form["userlist"].ToString().Split(';');
            foreach (var userId in userIds)
            {
                if (userId == "")
                    continue;
                var user = await _userManager.FindByEmailAsync(userId);
                hive.Users.Add(
                    new HiveApplicationUser
                    {
                        Hive = hive,
                        HiveId = hive.LocationId,
                        ApplicationUser = user,
                        ApplicationUserId = user.Id
                    });
            }

            var suser = await _userManager.GetUserAsync(User);
            var location = _context.Locations.Find(hive.LocationId);
            hive.Location = location;
            hive.CreatorId = _userManager.GetUserId(User);
            location.Hives.Add(hive);
            hive.Users.Add(new HiveApplicationUser
            {
                ApplicationUser = suser,
                Hive = hive
            });
            _context.Update(location);
            _context.Hives.Add(hive);
            await _context.SaveChangesAsync();
            return CreatedAtAction("GetHive", new {id = hive.HiveId}, hive);
        }

        // DELETE: api/HivesApi/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteHive([FromRoute] int id)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var hive = await _context.Hives.FindAsync(id);
            if (hive == null) return NotFound();

            _context.Hives.Remove(hive);
            await _context.SaveChangesAsync();

            return Ok(hive);
        }

        private bool HiveExists(int id)
        {
            return _context.Hives.Any(e => e.HiveId == id);
        }
    }
}