﻿using System;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using HiveCare1.Data;
using HiveCare1.Models;
using HiveCare1.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace HiveCare1.Controllers
{
    public class BeekeepingController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly ApiariesController _apiariesController;
        private readonly UserService _userService;
        private readonly ApplicationUserManager _userManager;

        public BeekeepingController(ApplicationDbContext context, ApiariesController apiariesController,
            UserService userService, ApplicationUserManager userManager)
        {
            _context = context;
            _apiariesController = apiariesController;
            _userService = userService;
            _userManager = userManager;
        }

        public IActionResult Index(int? apiaryid, int? locationid, int? hiveid)
        {

            ViewData["ApiaryId"] = apiaryid;
            ViewData["LocationId"] = locationid;
            ViewData["HiveId"] = hiveid;

            return View();
        }

        [HttpPost("AddObservation")]
        [DisableRequestSizeLimit]
        public async Task<IActionResult> AddObservation([FromForm] Observation observation, int? hiveid)
        {

            var hive = await _context.Hives.FirstOrDefaultAsync(x => x.HiveId == observation.HiveId);
            if (hive == null) return NotFound();
            observation.Creator = await _userManager.GetCurrentUserAsync();
            observation.CreationDate = DateTime.Now;
            observation.Hive = hive;
            observation.HiveId = hiveid ?? 0;
            observation.Hive.Observations.Add(observation);
            _context.Add(observation);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index", "Beekeeping", new
            {
                apiaryid = observation.Hive.Location.Apiary.ApiaryId,
                locationid = observation.Hive.Location.LocationId,
                hiveid = observation.Hive.HiveId,
                slideactive = "observation",
            });

        }

        [HttpPost("AddFeed")]
        [DisableRequestSizeLimit]
        public async Task<IActionResult> AddFeed([FromForm] Feed feed, int? hiveid)
        {

            var hive = await _context.Hives.FirstOrDefaultAsync(x => x.HiveId == feed.HiveId);
            if (hive == null) return NotFound();
            feed.Creator = await _userManager.GetCurrentUserAsync();
            feed.CreationDate = DateTime.Now;
            feed.Hive = hive;
            feed.HiveId = hiveid ?? 0;
            feed.Hive.Feeds.Add(feed);
            _context.Add(feed);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index", "Beekeeping", new
            {
                apiaryid = feed.Hive.Location.Apiary.ApiaryId,
                locationid = feed.Hive.Location.LocationId,
                hiveid = feed.Hive.HiveId,
                slideactive = "feed",
            });

        }

        [HttpPost("AddProcess")]
        [DisableRequestSizeLimit]
        public async Task<IActionResult> AddProcess([FromForm] Process process, int? hiveid)
        {
            var hive = await _context.Hives.FirstOrDefaultAsync(x => x.HiveId == process.HiveId);
            if (hive == null) return NotFound();
            process.Creator = await _userManager.GetCurrentUserAsync();
            process.CreationDate = DateTime.Now;
            process.Hive = hive;
            process.HiveId = process.HiveId;
            process.Hive.Processes.Add(process);
            _context.Add(process);
            await _context.SaveChangesAsync();

            return RedirectToAction("Index", "Beekeeping", new
            {
                apiaryid = process.Hive.Location.Apiary.ApiaryId,
                locationid = process.Hive.Location.LocationId,
                hiveid = process.Hive.HiveId,
                slideactive = "process",
            });
        }

        [HttpPost("AddHiveYield")]
        [DisableRequestSizeLimit]
        public async Task<IActionResult> AddHiveYield([FromForm] HiveYieldAmount yieldAmount, int? hiveid)
        {
            var hive = await _context.Hives.FirstOrDefaultAsync(x => x.HiveId == yieldAmount.HiveId);
            if (hive == null) return NotFound();
            yieldAmount.Creator = await _userManager.GetCurrentUserAsync();
            yieldAmount.CreationDate = DateTime.Now;
            yieldAmount.Hive = hive;
            yieldAmount.HiveId = yieldAmount.HiveId;
            yieldAmount.Hive.YieldAmounts.Add(yieldAmount);
            _context.Add(yieldAmount);
            await _context.SaveChangesAsync();

            return RedirectToAction("Index", "Beekeeping", new
            {
                apiaryid = yieldAmount.Hive.Location.Apiary.ApiaryId,
                locationid = yieldAmount.Hive.Location.LocationId,
                hiveid = yieldAmount.Hive.HiveId,
                slideactive = "@yield",
            });
        }

        [HttpPost("AddTreatment")]
        [DisableRequestSizeLimit]
        public async Task<IActionResult> AddTreatment([FromForm] Treatment treatment, int? hiveid)
        {
            var hive = await _context.Hives.FirstOrDefaultAsync(x => x.HiveId == treatment.HiveId);
            if (hive == null) return NotFound();
            treatment.Creator = await _userManager.GetCurrentUserAsync();
            treatment.CreationDate = DateTime.Now;
            treatment.Hive = hive;
            treatment.HiveId = treatment.HiveId;
            treatment.Hive.Treatments.Add(treatment);
            _context.Add(treatment);
            await _context.SaveChangesAsync();

            return RedirectToAction("Index", "Beekeeping", new
            {
                apiaryid = treatment.Hive.Location.Apiary.ApiaryId,
                locationid = treatment.Hive.Location.LocationId,
                hiveid = treatment.Hive.HiveId,
                slideactive = "process",
            });
        }



    }
}