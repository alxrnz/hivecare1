﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace HiveCare1
{
    public enum HiveTypes
        {
            Jungvolk,
            Wirtschaftsvolk,
            Ableger,
            Schwarm,
            Aufgelöst
        }
    public class Program
    {
     
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args).ConfigureAppConfiguration((hostingContext, config) =>
                {
                    config.AddJsonFile("config/roles.json",
                        optional: false,        // File is not optional.
                        reloadOnChange: false);
                })
                .UseStartup<Startup>();

    }
}
