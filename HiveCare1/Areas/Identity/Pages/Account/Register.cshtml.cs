﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using HiveCare1.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace HiveCare1.Areas.Identity.Pages.Account
{
    public class RegisterModel : PageModel
    {
        private readonly IEmailSender _emailSender;
        private readonly ILogger<RegisterModel> _logger;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ApplicationUserManager _userManager;
        private readonly ApiariesController _apiariesController;
        private readonly RoleManager<ApplicationRole> _roleManager;

        public RegisterModel(
            ApplicationUserManager userManager,
            SignInManager<ApplicationUser> signInManager,
            ILogger<RegisterModel> logger,
            IEmailSender emailSender,
            ApiariesController apiariesController,
            RoleManager<ApplicationRole> roleManager
            )
        {

            _roleManager = roleManager;
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
            _emailSender = emailSender;
            _apiariesController = apiariesController;
        }

        [BindProperty] public InputModel Input { get; set; }

        public string ReturnUrl { get; set; }

        public void OnGet(string returnUrl = null)
        {
            ReturnUrl = returnUrl;
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {
            returnUrl = returnUrl ?? Url.Content("~/");
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser
                {
                    UserName = Input.Email,
                    Email = Input.Email,
                    Zip = Input.Zip,
                    Firstname = Input.Firstname,
                    Familyname = Input.Familyname,
                    City = Input.City,
                    Street = Input.Street,
                    HouseNo = Input.HouseNo
                };
                var apiaries = Input.Apiaries;
                var adminuser = await _userManager.GetUserAsync(User);
                foreach (var apiaryId in apiaries)
                {
                    var tmpapiary =
                        (await _apiariesController.ListApiariesAsync()).First(apiary => apiary.ApiaryId == apiaryId);
                    if(tmpapiary == null) continue;
                    user.Apiaries.Add(new ApiaryApplicationUser()
                    {
                        ApplicationUser = user,
                        Apiary = tmpapiary,
                        ApiaryId = apiaryId,
                    });
                }


                var result = await _userManager.CreateAsync(user, Input.Password);
                if (result.Succeeded)
                {
                    foreach (var inputRole in Input.Roles)
                    {
                        await _userManager.AddToRoleAsync(user, inputRole);
                    }
                    _logger.LogInformation("User created a new account with password.");

                    var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                    var callbackUrl = Url.Page(
                        "/Account/ConfirmEmail",
                        null,
                        new {userId = user.Id, code},
                        Request.Scheme);

                    await _emailSender.SendEmailAsync(Input.Email, "Confirm your email",
                        $"Please confirm your account by <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>clicking here</a>.");

                    await _signInManager.SignInAsync(user, false);
                    return LocalRedirect(returnUrl);
                }

                foreach (var error in result.Errors) ModelState.AddModelError(string.Empty, error.Description);
            }

            // If we got this far, something failed, redisplay form
            return Page();
        }

        public class InputModel
        {
            [Required]
            [EmailAddress]
            [Display(Name = "Email")]
            public string Email { get; set; }

            [Required]
            [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.",
                MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = "Passwort")]
            public string Password { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "Passwort bestätigen")]
            [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
            public string ConfirmPassword { get; set; }

            [Required]
            [Display(Name = "Vorname")]
            public string Firstname { get; set; }

            [Required] [Display(Name = "Name")] public string Familyname { get; set; }

            [Required] [Display(Name = "Straße")] public string Street { get; set; }

            [Required] [Display(Name = "Hausnummer")] public string HouseNo { get; set; }

            [Required] [Display(Name = "Stadt")] public string City { get; set; }

            [Required] [Display(Name = "PLZ")] public string Zip { get; set; }

            [Required] [Display(Name = "Rollen")] public List<string> Roles { get; set; }
            [Display(Name = "Imkereien")] public List<int> Apiaries { get; set; }

            [Display(Name = "Standorte")] public List<int> Locations { get; set; }
            [Display(Name = "Bienenvölker")] public List<int> Hives { get; set; }
        }
    }
}