﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using HiveCare1.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace HiveCare1.Areas.Identity.Pages.AdminPage
{
    [Authorize(Policy = "RequireSuperuserRole")]
    public class ManageUsersModel : PageModel
    {
        private readonly IEmailSender _emailSender;
        private readonly ILogger<ManageUsersModel> _logger;
        private readonly RoleManager<ApplicationRole> _roleManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ApplicationUserManager _userManager;

        public ManageUsersModel(ApplicationUserManager userManager,
            SignInManager<ApplicationUser> signInManager,
            ILogger<ManageUsersModel> logger,
            IEmailSender emailSender,
            RoleManager<ApplicationRole> roleManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
            _emailSender = emailSender;
            _roleManager = roleManager;

            initUsersAndRoles();
        }

        public string ReturnUrl { get; set; }
        public List<ApplicationRole> Roles { get; set; } = new List<ApplicationRole>();

        public Dictionary<string, IList<ApplicationUser>> Users { get; set; } =
            new Dictionary<string, IList<ApplicationUser>>();

        public string SelectedRole { get; set; }
        [BindProperty] public InputModel Input { get; set; }

        private async void initUsersAndRoles()
        {
            foreach (var applicationRole in _roleManager.Roles)
            {
                var users = await _userManager.GetUsersInRoleAsync(applicationRole.Name);
                Users.TryAdd(applicationRole.Name, users);
            }
            var roles = _roleManager.Roles;
            Roles = roles.ToList();
        }

        public void OnGet(string returnUrl = null)
        {
            ReturnUrl = returnUrl;
            initUsersAndRoles();
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {
            returnUrl = returnUrl ?? "/Identity/AdminManage/ManageUsers";
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser
                {
                    UserName = Input.Email,
                    Email = Input.Email,
                    Zip = Input.Zip,
                    Firstname = Input.Firstname,
                    Familyname = Input.Familyname,
                    City = Input.City,
                    Street = Input.Street,
                    HouseNo = Input.HouseNo
                };
                var result = await _userManager.CreateAsync(user, Input.Password);
                if (result.Succeeded)
                {
                    _logger.LogInformation("User created a new account with password.");
                    await _userManager.AddToRoleAsync(user, Input.Role);
                    var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                    var callbackUrl = Url.Page(
                        "/Account/ConfirmEmail",
                        null,
                        new {userId = user.Id, code},
                        Request.Scheme);

                    await _emailSender.SendEmailAsync(Input.Email, "Confirm your email",
                        $"Please confirm your account by <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>clicking here</a>.");

                    return RedirectToPage();
                }

                foreach (var error in result.Errors) ModelState.AddModelError(string.Empty, error.Description);
            }

            // If we got this far, something failed, redisplay form
            return Page();
        }

        public class InputModel
        {
            [Required]
            [EmailAddress]
            [Display(Name = "Email")]
            public string Email { get; set; }

            [Required]
            [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.",
                MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = "Password")]
            public string Password { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "Confirm password")]
            [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
            public string ConfirmPassword { get; set; }

            [Required]
            [Display(Name = "First Name")]
            public string Firstname { get; set; }

            [Required] [Display(Name = "Name")] public string Familyname { get; set; }

            [Required] [Display(Name = "Street")] public string Street { get; set; }

            [Required] [Display(Name = "Number")] public string HouseNo { get; set; }

            [Required] [Display(Name = "City")] public string City { get; set; }

            [Required] [Display(Name = "ZIP")] public string Zip { get; set; }

            [Required] [Display(Name = "Role")] public string Role { get; set; } = "Beekeeper";
        }
    }
}