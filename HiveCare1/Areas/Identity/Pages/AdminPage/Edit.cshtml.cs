﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using HiveCare1.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace HiveCare1.Areas.Identity.Pages.AdminPage
{
    public class EditModel : PageModel
    {
        private readonly ApplicationUserManager _userManager;


        public EditModel(
            ApplicationUserManager userManager)
        {
            _userManager = userManager;
        }

        //public EditModel(
        //    ApplicationUserManager userManager,
        //    ApplicationUsers user)
        //{
        //    _userManager = userManager;
        //    Input = new InputModel();
        //    Input.Email = user.Email;
        //    Input.City = user.City;
        //    Input.ConfirmPassword = user.Password;
        //    Input.Password = user.Password;
        //    Input.Familyname = user.Familyname;
        //    Input.Firstname = user.Firstname;
        //    Input.HouseNo = user.HouseNo;
        //    Input.Street = user.Street;
        //    Input.Zip = Input.Zip;
        //}

        [BindProperty] public InputModel Input { get; set; }

        public string ReturnUrl { get; set; }

        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {
            returnUrl = returnUrl ?? Url.Content("~/");
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(Input.Email);
                if (user == null)
                {
                    return BadRequest();
                }
                user.UserName = Input.Email;
                user.Email = Input.Email ?? user.Email;
                user.Zip = Input.Zip?? user.Zip;
                user.Firstname = Input.Firstname ?? user.Firstname;
                user.Familyname = Input.Familyname ?? user.Familyname;
                user.City = Input.City ?? user.City;
                user.Street = Input.Street ?? user.Street;
                user.HouseNo = Input.HouseNo ?? user.HouseNo;
                var result = await _userManager.UpdateAsync(user);
                if (result.Succeeded) return LocalRedirect(returnUrl);

                foreach (var error in result.Errors) ModelState.AddModelError(string.Empty, error.Description);
            }

            // If we got this far, something failed, redisplay form
            return Page();
        }

        public async Task<IActionResult> OnGetAsync(string email)
        {
            var user = await _userManager.FindByEmailAsync(email);
            if (user == null) return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");

            Input = new InputModel
            {
                Email = email,
                City = user.City,
                Street = user.Street,
                Familyname = user.Familyname,
                Firstname = user.Firstname,
                Zip = user.Zip,
                HouseNo = user.HouseNo
            };

            return Page();
        }

        public class InputModel
        {
            [EmailAddress]
            [Display(Name = "Email")]
            public string Email { get; set; }

            [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.",
                MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = "Password")]
            public string Password { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "Confirm password")]
            [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
            public string ConfirmPassword { get; set; }

            [Display(Name = "First Name")]
            public string Firstname { get; set; }

            [Display(Name = "Name")] public string Familyname { get; set; }

            [Display(Name = "Street")] public string Street { get; set; }

            [Display(Name = "Number")] public string HouseNo { get; set; }

            [Display(Name = "City")] public string City { get; set; }

            [Display(Name = "ZIP")] public string Zip { get; set; }
            [Display(Name = "Imkereien")] public List<int> Apiaries { get; set; }

            [Display(Name = "Standorte")] public List<int> Locations { get; set; }
            [Display(Name = "Bienenvölker")] public List<int> Hives { get; set; }
            public ApplicationUser Creator { get; set; }
        }
    }
}