﻿class HiveCare {

    
    locationPicker(sender, height, width) {
        
        var formId = $(sender).parents().closest("form").attr("id");
        if (height == undefined)
            height = $(`#${formId}`).height() + "px";
        if (width == undefined)
            width = $(`#${formId}`).width() + "px";
        if ($(`#${formId}_LocationPicker`).length == 0) {
            const container = document.createElement("div");
            container.setAttribute("class", "container-fluid");
            container.setAttribute("id", formId + "_LocationPicker");
            container.setAttribute("style", `width:${width}; height:${height};z-index:500;`);
            const closeBtn = document.createElement("div");
            closeBtn.setAttribute("style",
                "position: absolute; z-index:500;width:25px;height:25px;top:5px;right:5px;border-radius:10px;background-color: rgba(255,61,61,0.41);cursor:pointer");
            closeBtn.setAttribute("data-toggle", "tooltip");
            closeBtn.setAttribute("data-placement", "top");
            closeBtn.setAttribute("role", "button");
            closeBtn.setAttribute("title", "Schließen");
            closeBtn.innerHTML =
                '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path stroke="#FFFFFF" fill="#FFFFFF" d="M24 9.4L22.6 8 16 14.6 9.4 8 8 9.4l6.6 6.6L8 22.6 9.4 24l6.6-6.6 6.6 6.6 1.4-1.4-6.6-6.6L24 9.4z"/></svg>'

            $(`#${formId}`).prepend(container);


            let map = L.map(formId + "_LocationPicker", { scrollWheelZoom: true })
                .setView([48.770, 9.322], 12);
            if (hivecare.options.mapbox_token === "" || hivecare.options.mapbox_token == undefined) {
                L.tileLayer("https://{s}.tile.openstreetmap.de/tiles/osmde/{z}/{x}/{y}.png",
                    {
                        maxZoom: 24,
                        attribution:
                            '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
                    }).addTo(map);
            } else {
                L.tileLayer("https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}",
                    {
                        attribution:
                            'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
                        maxZoom: 24,
                        id: "mapbox.streets",
                        accessToken: hivecare.options.mapbox_token
                    }).addTo(map);
            }
            $(closeBtn).on("click",
                function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                    $(container).hide();
                });
            map.on("click", function (e) {
                hivecare.getAddressFromLatLon(e.latlng.lat,
                    e.latlng.lng,
                    function (data) {
                        var formid = `#${formId}`;
                        var latitudeInput = $(formid).find("#Latitude");
                        var longitudeInput = $(formid).find("#Longitude");
                        var streetInput = $(formid).find("#Street");
                        var housenrInput = $(formid).find("#HouseNo");
                        var zipInput = $(formid).find("#Zip");
                        var cityInput = $(formid).find("#City");
                        latitudeInput.val(data.lat);
                        longitudeInput.val(data.lng);
                        streetInput.val(data.road);
                        housenrInput.val(data.house_number);
                        zipInput.val(data.postcode);
                        cityInput.val((data.city == undefined
                            ? (data.town == undefined ? data.village : data.town)
                            : data.city));

                        hivecare.setCurrentMarkerToLatLng(map, e.latlng.lat, e.latlng.lng);
                        if (hivecare.activeMarker != null) {
                            hivecare.activeMarker.bindPopup(
                                `<span><b>${data.road} ${data.house_number}</b><br>${data.postcode} ${data.city == undefined
                                ? (data.town == undefined ? data.village : data.town)
                                : data.city}<br>${data.state}, ${data.county
                                }<br><span style='font-size:smaller;'>@${data.lat};${data.lng}</span></span>`).openPopup();
                        }
                        //$(container).hide();

                    });
            });
            $(container).prepend(closeBtn);
        } else {
            $(`#${formId}_LocationPicker`).show();
            setTimeout(function () { map.invalidateSize() }, 500);
        }
        
        
    }
    showMap(mapId, options, override) {

        if (mapId == undefined) {
            if (hivecare.mapId == undefined)
                return;
        } else {
            hivecare.mapId = mapId;
        }
        if (options.height == undefined) {
            options.height = hivecare.options.height == undefined
                ? window.innerHeight - 200
                : hivecare.options.height;
        }
        if (options.latLon == undefined) {
            options.latLon = hivecare.options.latLon == undefined
                ? { lat: 48.770, lon: 9.322 }
                : hivecare.options.latLon;
        }

        if (options.type == undefined) {
            options.type = "";
        }
        hivecare.options.type = options.type;

        if (options.action == undefined) {
            options.action = "";
        }
        hivecare.options.action = options.action;
        if (options.mapbox_token == undefined) {
            options.mapbox_token = "";
        }
        if (options.feed == undefined) {
            options.feed = ["apiaries", "alerts", "hives", "users", "locations"];
        }
        if (options.scrollWheelZoom == undefined) {
            options.scrollWheelZoom = false;
        }
        if (override === true)
            hivecare.options = options;
        hivecare.activeMarker = null;


        $(`#${hivecare.mapId}`).height(hivecare.options.height);
        this.createMapWrapper();
        this.map = L.map(hivecare.mapId, { scrollWheelZoom: false })
            .setView([hivecare.options.latLon.lat, hivecare.options.latLon.lon], 8);
        this.createMarkerGroups();

        this.feedMarkersAsync();

        this.bindEvents();
        if (hivecare.options.mapbox_token === "" || hivecare.options.mapbox_token == undefined) {
            L.tileLayer("https://{s}.tile.openstreetmap.de/tiles/osmde/{z}/{x}/{y}.png",
                {
                    maxZoom: 18,
                    attribution:
                        '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
                }).addTo(hivecare.map);
        } else {
            L.tileLayer("https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}",
                {
                    attribution:
                        'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
                    maxZoom: 18,
                    id: "mapbox.streets",
                    accessToken: hivecare.options.mapbox_token
                }).addTo(this.map);
        }
    }

    constructor(mapId, options) {
        if (options.height == undefined) {
            options.height = window.innerHeight - 200;
        }
        if (options.latLon == undefined) {
            options.latLon = { lat: 48.770, lon: 9.322 };
        }
        if (options.mapbox_token == undefined) {
            options.mapbox_token = "";
        }
        this.options = options;
        if ($(".forceMap").length > 0)
            mapId = $(".forceMap").attr("id");

        this.mapId = mapId;
        this.activeMarker = null;

    }

    bindEvents() {
        this.EventListenerPseudo = document.createElement("i");
        document.body.appendChild(this.EventListenerPseudo);
        $(this.EventListenerPseudo).on("hcAddressResultEvent",
            function(ev, data) {
                if (hivecare.activeMarker != null) {
                    hivecare.activeMarker.bindPopup(
                        `<span><b>${data.road} ${data.house_number}</b><br>${data.postcode} ${data.city == undefined
                        ? (data.town == undefined ? data.village : data.town)
                        : data.city}<br>${data.state}, ${data.county
                        }<br><span style='font-size:smaller;'>@${data.lat};${data.lng}</span></span>`).openPopup();
                }

            });
        this.map.on("click", this.onMapClick);

        this.mapFilters.forEach(function(el, index) {
            $(".hcFilterBarBtn ").on("click",
                function(ev) {
                    ev.preventDefault();
                    ev.stopPropagation();
                    const btn = $(ev.target);
                    const val = btn.attr("value");
                    const active = btn.hasClass("active");
                    const group = hivecare.markerGroups[val];
                    if (active) {
                        btn.removeClass("active");
                        if (hivecare.map.hasLayer(group))
                            hivecare.map.removeLayer(group);
                    } else {
                        btn.addClass("active");
                        if (!hivecare.map.hasLayer(group))
                            hivecare.map.addLayer(group);
                    }

                });
        });
    }

    getIconUrl(type) {
        return document.location.origin + "/images/" + type + ".svg";
    }

    createMarkerIcon(type) {
        const url = this.getIconUrl(type);
        return new L.Icon({
            iconUrl: url,
            iconSize: [38, 95], // size of the icon
            iconAnchor: [22, 94], // point of the icon which will correspond to marker's location
            popupAnchor: [-3, -76]
        });
    }

    createMarkerGroups() {
        this.markerGroups = {};
        const feed = this.options.feed;

        for (let index in feed) {
            if (feed.hasOwnProperty(index)) {
                const type = feed[index];
                this.markerGroups[type] = L.layerGroup();
                this.markerGroups[type].addTo(this.map);
            }
        }
    }

    feedMarkerSendAjax(type) {
        const group = this.markerGroups[type];
        const icon = this.createMarkerIcon(type);
        if (group != undefined) {
            const controller = type;
            const controllerName = `${controller.charAt(0).toUpperCase()}${controller.slice(1)}`;
            const url =
                `/api/${controllerName}Api/GetUser${controllerName}Async`;
            $.ajax({
                type: "GET",
                url: url,
                context: document.body
            }).done(function(data) {
                data.forEach(function(element, index) {
                    if (element.latitude != undefined && element.longitude != undefined) {
                        const marker = new L.marker([element.latitude, element.longitude],
                            { icon: icon });
                        marker.hivecareElement = element.name;
                        marker.bindPopup(
                            `<div id="${element.name.replace(" ", "")
                            }"class='container-fluid'><div class='row'><div class='col-12'><h4>${element.name
                            }</h4></div><div class='col-4'>Stadt</div><div class='col-8'>${element.city
                            }</div><div class='col-4'>Straße</div><div class='col-8'>${element.street} ${element
                            .houseNo
                            }</div><div class='col-4'>Ersteller</div><div class='col-8'>${element.creator
                            .familyname
                            }, ${element.creator.firstname
                            }</div><div class='col-12'><span style='font-size:smaller;'>@${element.latitude};${
                            element.longitude}</span><a href="/${controllerName}/Edit/${element.id
                            }">bearbeiten</a></div></div></div>`);
                        marker.on("click",
                            function(e) {
                                this.openPopup();
                            });
                        group.addLayer(marker);
                    }
                });
            }).fail(function(jqXHR, textStatus, errorThrown) {

            }).always(function(jqXHR, textStatus, errorThrown) {
                console.log(`status: ${textStatus}: ${type}`);
            });
        }
    }

    feedMarkersAsync() {
        const feed = this.options.feed;

        for (let index in feed) {
            if (feed.hasOwnProperty(index)) {
                const type = feed[index];
                this.feedMarkerSendAjax(type);
            }
        }

    }

    createMapWrapper() {
        const mapelemOuter = document.createElement("div");
        mapelemOuter.setAttribute("class", $(`#${this.mapId}`).attr("class"));
        mapelemOuter.setAttribute("id", `container_${this.mapId}`);
        const mapelemContainer = document.createElement("div");
        mapelemContainer.setAttribute("class", "container-fluid");
        const mapelemRow = document.createElement("div");
        mapelemRow.setAttribute("class", "row");
        const filters = this.createFilterBar();
        const cloned = $(`#${this.mapId}`).first().prepend(filters).clone();
        const classname = cloned.attr("class")
            .replace(/\b((col-)((\w{2})-)?[0-9]+)/, "")
            .replace(/\b((offset-)((\w{2})-)?[0-9]+)/, "")
            .replace(/\b((push-)((\w{2})-)?[0-9]+)/, "")
            .replace(/\b((pull-)((\w{2})-)?[0-9]+)/, "") +
            "col-12";
        cloned.prepend(filters);
        cloned.appendTo(mapelemRow);
        cloned.attr("class", classname);
        mapelemContainer.appendChild(mapelemRow);
        mapelemOuter.appendChild(mapelemContainer);
        const maptoggle = this.createToggleBar();
        mapelemOuter.appendChild(maptoggle);
        $(`#${this.mapId}`).replaceWith(mapelemOuter);
    }

    createFilterBar() {
        const filters = Array();
        const bar = document.createElement("div");
        bar.setAttribute("class", "col-12");
        bar.setAttribute("id", "hcFilterBar");;
        const filtergroup = document.createElement("div");
        filtergroup.setAttribute("role", "group");
        filtergroup.setAttribute("class", "btn-group");
        filtergroup.setAttribute("id", "hcFilterBarGrp");

        const apiariesFilter = this.filterbutton("Imkereien", "apiaries");
        filtergroup.appendChild(apiariesFilter);
        filters.push(apiariesFilter);

        const locationsFilter = this.filterbutton("Standorte", "locations");
        filtergroup.appendChild(locationsFilter);
        filters.push(locationsFilter);

        const hivesFilter = this.filterbutton("Bienenvölker", "hives");
        filtergroup.appendChild(hivesFilter);
        filters.push(hivesFilter);

        const alertsFilter = this.filterbutton("Warnungen", "alerts");
        filtergroup.appendChild(alertsFilter);
        filters.push(alertsFilter);

        const usersFilter = this.filterbutton("Benutzer", "users");
        filtergroup.appendChild(usersFilter);
        filters.push(usersFilter);


        bar.appendChild(filtergroup);
        this.mapFilters = filters;
        return bar;
    }

    filterbutton(text, type) {
        const alertsFilter = document.createElement("button");
        alertsFilter.setAttribute("class", "btn btn-dark hcFilterBarBtn active");
        alertsFilter.setAttribute("type", "button");
        alertsFilter.innerText = text;
        alertsFilter.setAttribute("value", type);
        return alertsFilter;
    }

    createToggleButton() {
        const button = document.createElement("a");
        button.setAttribute("id", "hcMapToggler");
        button.setAttribute("minimized", "false");
        button.setAttribute("restoreHeight", $(`#${this.mapId}`).height());
        button.innerHTML =
            '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path d="M16 22L6 12l1.4-1.4 8.6 8.6 8.6-8.6L26 12z"/></svg>';
        button.addEventListener("click",
            function(e) {
                if (document.getElementById("hcMapToggler").getAttribute("minimized") === "false") {
                    $("#hcMapToggler").addClass("down");

                    $(`#${hivecare.mapId}`).animate({ "height": 25 },
                        500,
                        function() {
                            document.getElementById("hcMapToggler").setAttribute("minimized", "true");
                            hivecare.map.invalidateSize();
                        });
                } else {
                    $("#hcMapToggler").removeClass("down");
                    $(`#${hivecare.mapId}`).animate(
                        { "height": document.getElementById("hcMapToggler").getAttribute("restoreHeight") },
                        500,
                        function() {

                            document.getElementById("hcMapToggler").setAttribute("minimized", "false");
                            hivecare.map.invalidateSize();
                        });
                }
            });
        return button;
    }

    createToggleButtonDiv() {
        const buttondiv = document.createElement("div");
        buttondiv.setAttribute("class", "d-flex");
        buttondiv.setAttribute("id", "hcMapTogglerContainer");
        return buttondiv;
    }

    createToggleBar() {
        const button = this.createToggleButton();
        const container = this.createToggleButtonDiv();
        container.appendChild(button);
        return container;
    }


    get GetMap() {
        return map;
    }

    onMapClick(e) {
        var type = hivecare.options.type;
        var method = hivecare.options.action;


        var group = hivecare.markerGroups[type];

        switch (method) {
        case "edit":
            var formid = `#${type.charAt(0).toUpperCase()}${type.slice(1)}Edit`;
            var latitudeInput = $(formid).find("#Latitude");
            var longitudeInput = $(formid).find("#Longitude");
            var streetInput = $(formid).find("#Street");
            var housenrInput = $(formid).find("#HouseNo");
            var zipInput = $(formid).find("#Zip");
            var cityInput = $(formid).find("#City");
            var marker;
            var elementname = $(formid).find("#Name");
            group.getLayers().forEach(function(layer, idx) {
                if (layer.hivecareElement = elementname) {
                    marker = layer;
                }
            });
            if (marker == undefined) {
                console.log("no marker found");
                return;
            }
            marker.setLatLng([e.latlng.lat, e.latlng.lng]);
            $(hivecare.EventListenerPseudo).on("hcAddressResultEvent",
                function(ev, data) {
                    streetInput.val(data.road);
                    housenrInput.val(data.house_number);
                    zipInput.val(data.postcode);
                    cityInput.val((data.city == undefined
                        ? (data.town == undefined ? data.village : data.town)
                        : data.city));
                });
            hivecare.getAddressFromLatLon(e.latlng.lat, e.latlng.lng);
            hivecare.setLatLngFormField(e.latlng.lat, latitudeInput, e.latlng.lng, longitudeInput);
            hivecare.setCurrentMarkerToLatLng(hivecare.map,e.latlng.lat, e.latlng.lng, group);
            break;
        case "create":
            var formid = `#${type.charAt(0).toUpperCase()}${type.slice(1)}Create`;
            var latitudeInput = $(formid).find("#Latitude");
            var longitudeInput = $(formid).find("#Longitude");
            var streetInput = $(formid).find("#Street");
            var housenrInput = $(formid).find("#HouseNo");
            var zipInput = $(formid).find("#Zip");
            var cityInput = $(formid).find("#City");
            $(hivecare.EventListenerPseudo).on("hcAddressResultEvent",
                function(ev, data) {
                    streetInput.val(data.road);
                    housenrInput.val(data.house_number);
                    zipInput.val(data.postcode);
                    cityInput.val((data.city == undefined
                        ? (data.town == undefined ? data.village : data.town)
                        : data.city));
                });
            hivecare.getAddressFromLatLon(e.latlng.lat, e.latlng.lng);
            hivecare.setLatLngFormField(e.latlng.lat, latitudeInput, e.latlng.lng, longitudeInput);
                hivecare.setCurrentMarkerToLatLng(hivecare.map,e.latlng.lat, e.latlng.lng, group);
            break;

        default:
            if (hivecare.activeMarkerExists()) {
                hivecare.map.removeLayer(hivecare.activeMarker);
                hivecare.activeMarker = null;
            } else {
                hivecare.setCurrentMarkerToLatLng(hivecare.map,e.latlng.lat, e.latlng.lng, group);
                hivecare.getAddressFromLatLon(e.latlng.lat, e.latlng.lng);
            }
            break;
        }
    }

    activeMarkerExists() {
        if (hivecare.activeMarker == null || hivecare.activeMarker == undefined) return false;
        return true;
    }

    setCurrentMarkerToLatLng(map,lat, lng, group) {

        if (!this.activeMarkerExists()) {
            hivecare.activeMarker = new L.marker([lat, lng]);
            if (group != undefined) {
                group.addLayer(hivecare.activeMarker);
            } else {
                map.addLayer(hivecare.activeMarker);
            }
        } else {

            hivecare.activeMarker.setLatLng([lat, lng]);
            hivecare.activeMarker.addTo(map);
        }
    }

    setLatLngFormField(lat, elemlat, lng, elemlng) {
        elemlat.val((lat + "").replace(".", ","));
        elemlng.val((lng + "").replace(".", ","));
    }

    getLocationTableRow(location) {
        const tablerow = document.createElement("tr");

        const Name = document.createElement("td");
        Name.innerText = location.name;

        const LatLon = document.createElement("td");
        LatLon.innerText = location.latitude + ";" + location.longitude;

        const LocMan = document.createElement("td");
        LocMan.innerText = location.administrators.length;

        const Hives = document.createElement("td");
        Hives.innerText = location.hives.length;

        tablerow.appendChild(Name);
        tablerow.appendChild(LatLon);
        tablerow.appendChild(LocMan);
        tablerow.appendChild(Hives);
        return tablerow;
    }

    getHivesTableRow(hive) {
        const tablerow = document.createElement("tr");

        const Name = document.createElement("td");
        Name.innerText = hive.name;

        const Imker = document.createElement("td");
        Imker.innerText = hive.administrators.length + ";" + hive.longitude;

        const Yields = document.createElement("td");
        Yields.innerText = 0;

        const Sum = document.createElement("td");
        Sum.innerText = 0;

        const Observations = document.createElement("td");
        Observations.innerText = hive.observations.length;

        tablerow.appendChild(Name);
        tablerow.appendChild(Imker);
        tablerow.appendChild(Yields);
        tablerow.appendChild(Sum);
        tablerow.appendChild(Observations);
        return tablerow;
    }

    moveMapToDiv(containierId, type, action) {
        if (type == undefined)
            type = $(`#${containierId}`).data("type");
        if (action == undefined)
            action = $(`#${containierId}`).data("action");
        hivecare.options.oldaction = hivecare.options.action;
        hivecare.options.action = action;
        hivecare.options.oldtype = hivecare.options.type;
        hivecare.options.type = type;
        $(`#${containierId}`).append($(`.leaflet-container,#hcMapTogglerContainer`));
        setTimeout(function() {
                hivecare.map.invalidateSize();
            },
            500);
    }

    moveMapToOrigin() {
        hivecare.options.action = hivecare.options.oldaction;
        hivecare.options.type = hivecare.options.oldtype;
        hivecare.options.oldaction = undefined;
        hivecare.options.oldtype = undefined;
        $(`#container_${this.mapId}`).append($(`.leaflet-container,#hcMapTogglerContainer`));
        setTimeout(function() {
                hivecare.map.invalidateSize();
            },
            500);
    }

    clearform(event) {
        var form = $(event.target).closest("form");
        form[0].reset();
    }

    sendAjaxForm(event, cbfunction) {
        event.preventDefault();
        const form = $(event.target).parents("form");
        var data = {};
        const formarray = $(form).serializeArray();
        $.each(formarray,
            function() {
                if (this.name !== "__RequestVerificationToken")
                    if (data[this.name]) {
                        if (!data[this.name].push) {
                            data[this.name] = [data[this.name]];
                        }
                        data[this.name].push(this.value || "");
                    } else {
                        data[this.name] = this.value || "";
                    }
            });
        $.ajax({
            type: "POST",
            url: form.prop("action"),
            data: data,
            dataType: "json"
        }).done(function(data) {
            cbfunction(data);
        });
    }

    getLatLonFromAddress(str, nr, zip, city) {
        //"https://nominatim.openstreetmap.org/search?q=17+Strada+Pictor+Alexandru+Romano%2C+Bukarest&format=geojson";
        $.ajax({
            type: "POST",
            url: "https://nominatim.openstreetmap.org/search",
            data: {
                q: nr + " " + str + " " + zip + " " + city,
                format: "geojson"
            },
            function(data) {
                console.log(data);
                return data;
            }
        });
    }

    getAddressFromLatLon(lat, lon, func) {
        //"https://nominatim.openstreetmap.org/search?q=17+Strada+Pictor+Alexandru+Romano%2C+Bukarest&format=geojson";
        /**
         https://nominatim.openstreetmap.org/reverse?format=json&lat=52.5487429714954&lon=-1.81602098644987&zoom=18&addressdetails=1
         */
        if ((lat + "").includes(","))
            lat = lat.replace(",", ".");
        if ((lon + "").includes(","))
            lon = lon.replace(",", ".");
        $.ajax({
            method: "GET",
            url: "https://nominatim.openstreetmap.org/reverse",
            data: {
                format: "geojson",
                lat: lat,
                lon: lon,
                addressdetails: 1
            }
        }).done(
            function(data) {
                const resultdata = data.features[0].properties.address;
                resultdata.lat = lat;
                resultdata.lng = lon;
                if (func != undefined) {
                    func(resultdata);
                } else
                    $(hivecare.EventListenerPseudo)
                        .trigger("hcAddressResultEvent", data.features[0].properties.address);
            }
        );
    }

    adduser(e) {
        const sendertext = $(e.target).parents("form").find("#Administrators option:selected").text();
        const sendervalue = $(e.target).parents("form").find("#Administrators").val();
        const newObj = { name: sendertext, value: sendervalue };
        const adminInputval = $(e.target).parents("form").find("#adminuserlist").val();
        const userexists = adminInputval.indexOf(sendervalue);


        if (userexists === -1) {
            newObj.value.forEach(function(index, element) {
                $(e.target).parents("form").find("#AdminList").append(
                    `<li>${newObj.name}<br><span style="display: inline-block; font-size: small"><b>Email: ${element
                    }</b></span></li>`);
                $(e.target).parents("form").find("#adminuserlist").val(adminInputval + element + ";");
            });
            
        }

    }

    getObjectsByEntity(type) {
        const url =
            `/api/${type}Api/GetUser${type}Async`;
        $.ajax({
            type: "GET",
            url: url,
            context: document.body
        }).done(function(data) {
            return data;
        });
    }

    getObjectsByEntityAndId(type,id) {
        const url =
            `/api/${type}Api/GetUser${type}Async`;
        $.ajax({
            type: "GET",
            url: url,
            context: document.body
        }).done(function(data) {
            return data;
        });
    }
    doForEachObject(type,func) {
        const url =
            `/api/${type}Api/GetUser${type}Async`;
        $.ajax({
            type: "GET",
            url: url,
            context: document.body
        }).done(function(data) {
            return func(data);
        });
    }
    removeAdmin(e) {
        var card = $(e.target).closest(".hccard");
        var text = card.parents("form").find("#RemoveAdmin").val();
        if (text.indexOf(card.data("email")) === -1) {
            text += text.length > 0 ? `;${card.data("email")}` : card.data("email");
        }
        $('#RemoveAdmin').val(text);
        card.remove();
        
        console.log(e);
    }

}

var hivecare = hivecare || null;