﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HiveCare1.Areas.Identity;
using HiveCare1.Data;
using HiveCare1.Models;
using HiveCare1.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace HiveCare1
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
            services.AddScoped<ApplicationUserManager>();
            services.AddScoped<UserService>();
            services.AddAuthorization(options => options.AddPolicy("RequireSuperuserRole",
                policy => policy.RequireRole("Superuser")));

            services.AddAuthorization(options => options.AddPolicy("RequireAdministratorRole",
                policy => policy.RequireRole("Superuser,Administrator")));

            services.AddAuthorization(options => options.AddPolicy("RequireBeekeeperRole",
                policy => policy.RequireRole("Beekeeper")));


            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseLazyLoadingProxies()
                    .UseSqlite(Configuration.GetConnectionString("DefaultConnection"))
                );

            services.AddIdentity<ApplicationUser, ApplicationRole>()
                .AddRoles<ApplicationRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultUI()
                .AddDefaultTokenProviders();

            services.AddHttpContextAccessor();

            services.AddMvc().AddJsonOptions(options =>
            {
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            });

            services.AddMvc(config =>
            {
                var policy = new AuthorizationPolicyBuilder()
                    .RequireAuthenticatedUser()
                    .Build();
                config.Filters.Add(new AuthorizeFilter(policy));
            }).AddControllersAsServices().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddMvc()
                .AddRazorPagesOptions(options =>
                {
                    options.Conventions.AuthorizePage("/Identity/Account/Register", "RequireAdministratorRole");
                }).SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IServiceProvider serviceProvider,
            IConfiguration config, ApplicationDbContext context)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            context.Database.Migrate();
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    "default",
                    "{controller=Home}/{action=Index}/{id?}");
                /*routes.MapRoute(
                    "addobservation", 
                    "{controller=Beekeeping}/{action=AddObservation}/{id?}");*/
            });
            CreateUserRoles(serviceProvider, config).Wait();
            RegisterAdmin(serviceProvider).Wait();
            CreateTestusers(serviceProvider).Wait();
            CreateAndPopulateApitary(serviceProvider).Wait();
        }

        private async Task RegisterAdmin(IServiceProvider serviceProvider)
        {
            var userManager = serviceProvider.GetRequiredService<ApplicationUserManager>();
            var adminUser = Configuration.GetSection("SuperUser").Get<ApplicationUser>();
            adminUser.UserName = adminUser.Email;
            var user = await userManager.FindByNameAsync(adminUser.UserName);

            if (user == null)
            {
                var identityResult = await userManager.CreateAsync(adminUser, adminUser.Password);
                if (identityResult.Succeeded)
                    user = await userManager.FindByNameAsync(adminUser.UserName);
            }

            if (user != null)
            {
                var roleResult = await userManager.AddToRoleAsync(user, "SuperUser");
            }
        }

        private async Task CreateTestusers(IServiceProvider serviceProvider)
        {
            var userManager = serviceProvider.GetRequiredService<ApplicationUserManager>();
            for (var i = 0; i < 5; i++)
            {
                var testusr = new ApplicationUser();
                testusr.UserName = testusr.Email;
                testusr.City = "Bees";
                testusr.HouseNo = "127.0.0.1";
                testusr.Familyname = $"Maja die {i}.";
                testusr.Firstname = "Biene";
                testusr.Password = "Test123!";
                testusr.Street = "Blütenweg 3";
                testusr.Zip = "1337";
                testusr.Email = "test" + i + "@hive.care";
                testusr.UserName = "test" + i + "@hive.care";
                testusr.Roles = "Beekeeper";
                var applicationUser = await userManager.FindByNameAsync(testusr.UserName);

                if (applicationUser != null) continue;

                var identityResult = await userManager.CreateAsync(testusr, testusr.Password);
                applicationUser = await userManager.FindByNameAsync(testusr.UserName);


                if (applicationUser != null)
                {
                    var roleResult = await userManager.AddToRoleAsync(applicationUser, "Beekeeper");
                }
            }
        }

        private async Task CreateUserRoles(IServiceProvider serviceProvider, IConfiguration config)
        {
            var RoleManager = serviceProvider.GetRequiredService<RoleManager<ApplicationRole>>();


            var rolesConfig = Configuration.Get<ApplicationRolesConfig>();

            foreach (var role in rolesConfig.Roles)
            {
                IdentityResult roleResult;
                var roleCheck = await RoleManager.RoleExistsAsync(role.Role);
                if (!roleCheck)
                    roleResult = await RoleManager.CreateAsync(new ApplicationRole(role.Role, role.Description));
            }
        }

        private async Task CreateAndPopulateApitary(IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetRequiredService<ApplicationDbContext>();
            var userManager = serviceProvider.GetRequiredService<ApplicationUserManager>();
            
            context.Database.EnsureCreated();

            var superuser = (await userManager.GetUsersInRoleAsync("Superuser")).First();
            var apiaryAdminuser = (await userManager.GetUsersInRoleAsync("Beekeeper")).First( x => x != superuser);
            var locationAdminuser = (await userManager.GetUsersInRoleAsync("Beekeeper")).First(x => x != superuser && x != apiaryAdminuser);
            var hiveAdminuser = (await userManager.GetUsersInRoleAsync("Beekeeper")).First(x => x != superuser && x != apiaryAdminuser&& x != locationAdminuser);
            var hiveBeekeeper = (await userManager.GetUsersInRoleAsync("Beekeeper")).First(x => x != superuser && x != apiaryAdminuser && x != locationAdminuser&& x != hiveAdminuser);
            var exists = await context.Apiaries.FirstOrDefaultAsync(apiary1 => apiary1.Name == "Imkerle");
            if(exists != null) return;


            Apiary apiary = new Apiary()
            {
                Locations = new List<Location>(),
                Users = new List<ApiaryApplicationUser>(),
                ApiaryId = 0,
                CreationDate = DateTime.Now.AddYears(-5),
                Creator = superuser,
                CreatorId = superuser.Id,
                Name = "Imkerle"
            };
            apiary.Users.Add(
                new ApiaryApplicationUser()
                {
                    ApplicationUserId = superuser.Id,
                    ApplicationUser = superuser,
                    Apiary = apiary,
                    ApiaryId = apiary.ApiaryId,
                    IsAdmin = true
                });
            apiary.Users.Add(new ApiaryApplicationUser()
                {
                    ApplicationUserId = apiaryAdminuser.Id,
                    ApplicationUser = apiaryAdminuser,
                    Apiary = apiary,
                    ApiaryId = apiary.ApiaryId,
                    IsAdmin = true
                });
            Location location = new Location()
            {
                Users = new List<LocationApplicationUser>(),
                Apiary = apiary,
                ApiaryId = apiary.ApiaryId,
                LocationId = 0,
                Creator = superuser,
                CreatorId = superuser.Id,
                CreationDate = DateTime.Now.AddYears(-5),
                Hives = new List<Hive>(),
                Name = "Esslingen Hochschule",
                City = "Esslingen am Neckar",
                HouseNo = "",
                Latitude = 48.74093,
                Longitude = 9.34234,
                Street = "Bergwiesenweg",
                Zip = "73732",
            };
            location.Users.Add(
                new LocationApplicationUser()
                {
                    ApplicationUserId = superuser.Id,
                    ApplicationUser = superuser,
                    Location = location,
                    LocationId = location.ApiaryId,
                    IsAdmin = false
                });
            location.Users.Add(
                new LocationApplicationUser()
                {
                    ApplicationUserId = locationAdminuser.Id,
                    ApplicationUser = locationAdminuser,
                    Location = location,
                    LocationId = location.ApiaryId,
                    IsAdmin = true
                }
            );
            Hive hive = new Hive()
            {
                HiveId = 0,
                Name = "Volk 1",
                Location = location,
                LocationId = location.LocationId,
                Users = new List<HiveApplicationUser>(),
                Creator = superuser,
                CreatorId = superuser.Id,
                CreationDate = DateTime.Now.AddYears(-3),
                Queen = new Queen()
                {
                    QueenId = 0,
                    HiveId = 0,
                    Hive = null,
                    Creator = hiveAdminuser,
                    CreatorId = hiveAdminuser.CreatorId,
                    CreationDate = DateTime.Now,
                    QueenBirthYear = DateTime.Now.Year,
                    QueenPainted = true,
                },
                QueenId = 0,
                Aggressiveness = 3,
                Loyalty = 5,
                Inertia = 0,
                Gentleness = 5,
                Type = HiveTypeEnum.Wirtschaftsvolk,
                Feeds = new List<Feed>()
                {
                    new Feed()
                    {
                        Hive = null,
                        HiveId = 0,
                        Creator = hiveBeekeeper,
                        CreatorId = hiveBeekeeper.Id,
                        CreationDate = DateTime.Now,
                        Comment = "Simple but nice",
                        Amout = 500,
                        FeedId = 0,
                        FeedType = FeedTypeEnum.Futterteig,

                    },
                    new Feed()
                    {
                        Hive = null,
                        HiveId = 0,
                        Creator = hiveAdminuser,
                        CreatorId = hiveAdminuser.Id,
                        CreationDate = DateTime.Now.AddDays(-1),
                        Comment = "Yesterday Simple but nice",
                        Amout = 1.5,
                        FeedId = 0,
                        FeedType = FeedTypeEnum.Sirup,

                    }
                },
                Observations = new List<Observation>()
                {
                    new Observation()
                    {
                        HiveId = 0,
                        Hive = null,
                        Creator = hiveBeekeeper,
                        CreatorId = hiveBeekeeper.Id,
                        CreationDate = DateTime.Now.AddDays(-3),

                        Queen = true,
                        AirTraffic = AmountValueEnum.Viel,
                        Brood = true,
                        IsDone = true,
                        Comment = $"Beobachtung zum Test der Anwendung Ersteller ist {hiveBeekeeper.UserName}"
                    },
                    new Observation()
                    {
                        HiveId = 0,
                        Hive = null,
                        Creator = hiveAdminuser,
                        CreatorId = hiveAdminuser.Id,
                        CreationDate = DateTime.Now.AddDays(-1),

                        Queen = true,
                        AirTraffic = AmountValueEnum.Wenig,
                        Brood = true,
                        Varrora = true,
                        IsDone = true,
                        Comment = $"Beobachtung zum Test der Anwendung Ersteller ist {hiveAdminuser.UserName}"
                    }
                },
                Processes = new List<Process>(new[]
                {
                    new Process()
                    {
                        Creator = hiveBeekeeper,
                        CreatorId = hiveBeekeeper.CreatorId,
                        CreationDate = DateTime.Now.AddDays(-2),
                        DoneDateTime = DateTime.Now.AddDays(-2),
                        DueDateTime = DateTime.Now.AddDays(-3),

                        ProcessId = 0,
                        DronesFrames = 1,
                        BrutFrames = 2,
                        EmptyFrames = 1,
                        LiningFrames = 2,
                        MiddlewallFrame = 1,
                        HoneyBox = 3,
                        EmptyBox = 1,
                        BeesEscape = false,
                        Fence = true,
                        FlightholeWedge = FlightholeStatusEnum.Big,
                        Diaper = true,
                        IsDone = true,
                        Comment = $"Done but to late by {hiveBeekeeper}"
                    },
                    new Process()
                    {
                        Creator = hiveBeekeeper,
                        CreatorId = hiveBeekeeper.CreatorId,
                        CreationDate = DateTime.Now.AddDays(-2),
                        DoneDateTime = DateTime.Now.Date,
                        DoneBy = hiveAdminuser,

                        ProcessId = 0,
                        DronesFrames = 1,
                        BrutFrames = 0,
                        EmptyFrames = 2,
                        LiningFrames = 2,
                        MiddlewallFrame = 2,
                        HoneyBox = 1,
                        EmptyBox = 2,
                        BeesEscape = false,
                        Fence = true,
                        FlightholeWedge = FlightholeStatusEnum.Big,
                        Diaper = true,
                        IsDone = true,
                        Comment = $"Done {hiveBeekeeper}"
                    }
                }
                
                ),
                Treatments = new List<Treatment>(
                    new []
                    {
                        new Treatment()
                        {
                            Hive = null,
                            HiveId = 0,
                            Creator = superuser,
                            CreatorId = superuser.Id,
                            CreationDate = DateTime.Now,
                            
                            DueDateTime = DateTime.Now.AddDays(1).Date,
                            DoneDateTime = DateTime.Now.Date,
                            DoneBy = hiveAdminuser,

                            TypeOfAgenttype = TreatmentAgenttypeEnum.Ameisensäure,
                            BeePen = false,
                            Varroa = true,
                            Comment = "Milben behandlung"
                            
                        }, 
                    }),
                YieldAmounts = new List<HiveYieldAmount>()


            };
            hive.Users.Add(new HiveApplicationUser()
            {
                ApplicationUserId = hiveAdminuser.Id,
                ApplicationUser = hiveAdminuser,
                Hive = hive,
                HiveId = hive.HiveId,
                IsAdmin = true
            });

            Location location2 = new Location()
            {
                Users = new List<LocationApplicationUser>(),
                Apiary = apiary,
                ApiaryId = apiary.ApiaryId,
                LocationId = 0,
                Creator = superuser,
                CreatorId = superuser.Id,
                CreationDate = DateTime.Now.AddYears(-3),
                Hives = new List<Hive>(),
                Name = "Bienennest",
                City = "Esslingen am Neckar",
                HouseNo = "",
                Latitude = 48.717013,
                Longitude = 9.364355,
                Street = "Entennest",
                Zip = "73730",
            };
            location2.Users.Add(new LocationApplicationUser()
            {
                ApplicationUserId = superuser.Id,
                ApplicationUser = superuser,
                Location = location2,
                LocationId = location2.ApiaryId,
                IsAdmin = true
            });
                location2.Users.Add(new LocationApplicationUser()
                {
                    ApplicationUserId = locationAdminuser.Id,
                    ApplicationUser = locationAdminuser,
                    Location = location2,
                    LocationId = location2.ApiaryId,
                    IsAdmin = true
                });

            Hive hive2 = new Hive()
            {
                HiveId = 0,
                Name = "Volk 2",
                Location = location2,
                LocationId = location2.LocationId,
                Users = new List<HiveApplicationUser>(),
                Creator = superuser,
                CreatorId = superuser.Id,
                CreationDate = DateTime.Now.AddYears(-3),
                Queen = new Queen()
                {
                    QueenId = 0,
                    HiveId = 0,
                    Hive = null,
                    Creator = hiveAdminuser,
                    CreatorId = hiveAdminuser.CreatorId,
                    CreationDate = DateTime.Now,
                    QueenBirthYear = DateTime.Now.Year,
                    QueenPainted = true,
                },
                QueenId = 0,
                Aggressiveness = 0,
                Loyalty = 5,
                Inertia = 0,
                Gentleness = 5,
                Type = HiveTypeEnum.Wirtschaftsvolk,
                Feeds = new List<Feed>()
                {
                    new Feed()
                    {
                        Hive = null,
                        HiveId = 0,
                        Creator = hiveBeekeeper,
                        CreatorId = hiveBeekeeper.Id,
                        CreationDate = DateTime.Now,
                        Comment = "Simple but nice",
                        Amout = 33,
                        FeedId = 0,
                        FeedType = FeedTypeEnum.Futterteig,

                    },
                    new Feed()
                    {
                        Hive = null,
                        HiveId = 0,
                        Creator = hiveAdminuser,
                        CreatorId = hiveAdminuser.Id,
                        CreationDate = DateTime.Now.AddDays(-1),
                        Comment = "Yesterday Simple but nice",
                        Amout = 1.5,
                        FeedId = 0,
                        FeedType = FeedTypeEnum.Sirup,

                    }
                },
                Observations = new List<Observation>()
                {
                    new Observation()
                    {
                        HiveId = 0,
                        Hive = null,
                        Creator = hiveBeekeeper,
                        CreatorId = hiveBeekeeper.Id,
                        CreationDate = DateTime.Now.AddDays(-3),

                        Queen = true,
                        AirTraffic = AmountValueEnum.Viel,
                        Brood = true,
                        IsDone = true,
                        Comment = $"Beobachtung zum Test der Anwendung Ersteller ist {hiveBeekeeper.UserName}",
                        Liningcells = AmountValueEnum.Wenig,
                        Temperature = 20.5,
                        Weather = "Bewölkt/regnerisch"
                    },
                    new Observation()
                    {
                        HiveId = 0,
                        Hive = null,
                        Creator = hiveAdminuser,
                        CreatorId = hiveAdminuser.Id,
                        CreationDate = DateTime.Now.AddDays(-1),

                        Queen = true,
                        AirTraffic = AmountValueEnum.Wenig,
                        Brood = true,
                        Varrora = true,
                        IsDone = true,
                        Comment = $"Beobachtung zum Test der Anwendung Ersteller ist {hiveAdminuser.UserName}"
                    }
                },
                Processes = new List<Process>(new[]
                {
                    new Process()
                    {
                        Creator = hiveBeekeeper,
                        CreatorId = hiveBeekeeper.CreatorId,
                        CreationDate = DateTime.Now.AddDays(-2),
                        DoneDateTime = DateTime.Now.AddDays(-2),
                        DueDateTime = DateTime.Now.AddDays(-3),

                        ProcessId = 0,
                        DronesFrames = 1,
                        BrutFrames = 2,
                        EmptyFrames = 1,
                        LiningFrames = 2,
                        MiddlewallFrame = 1,
                        HoneyBox = 3,
                        EmptyBox = 1,
                        BeesEscape = false,
                        Fence = true,
                        FlightholeWedge = FlightholeStatusEnum.Big,
                        Diaper = true,
                        IsDone = true,
                        Comment = $"Done but to late by {hiveBeekeeper}"
                    },
                    new Process()
                    {
                        Creator = hiveBeekeeper,
                        CreatorId = hiveBeekeeper.CreatorId,
                        CreationDate = DateTime.Now.AddDays(-2),
                        DoneDateTime = DateTime.Now.Date,
                        DoneBy = hiveAdminuser,

                        ProcessId = 0,
                        DronesFrames = 1,
                        BrutFrames = 0,
                        EmptyFrames = 2,
                        LiningFrames = 2,
                        MiddlewallFrame = 2,
                        HoneyBox = 1,
                        EmptyBox = 2,
                        BeesEscape = false,
                        Fence = true,
                        FlightholeWedge = FlightholeStatusEnum.Big,
                        Diaper = true,
                        IsDone = true,
                        Comment = $"Done {hiveBeekeeper}"
                    }
                }
                
                ),
                Treatments = new List<Treatment>(
                    new []
                    {
                        new Treatment()
                        {
                            Hive = null,
                            HiveId = 0,
                            Creator = superuser,
                            CreatorId = superuser.Id,
                            CreationDate = DateTime.Now,
                            
                            DueDateTime = DateTime.Now.AddDays(1).Date,
                            DoneDateTime = DateTime.Now.Date,
                            DoneBy = hiveAdminuser,

                            TypeOfAgenttype = TreatmentAgenttypeEnum.Ameisensäure,
                            BeePen = false,
                            Varroa = true,
                            Comment = "Milben behandlung"
                            
                        }, 
                    }),
                YieldAmounts = new List<HiveYieldAmount>()
            };
            hive2.Users.Add(new HiveApplicationUser()
            {
                ApplicationUserId = hiveAdminuser.Id,
                ApplicationUser = hiveAdminuser,
                Hive = hive2,
                HiveId = hive2.HiveId,
                IsAdmin = true
            });
            Yield yield1 = new Yield()
            {
                YieldId = 0,
                CreationDate = DateTime.Now.AddYears(-1),
                Creator = hiveAdminuser,
                CreatorId = hiveAdminuser.Id,
                YieldType = YieldTypeEnum.Blütenhonig,
            };
            Yield yield2 = new Yield()
            {
                YieldId = 0,
                CreationDate = DateTime.Now,
                Creator = hiveAdminuser,
                CreatorId = hiveAdminuser.Id,
                YieldType = YieldTypeEnum.Sortenhonig,
                YieldTypeText = "Feinste Mischung"
            };
            hive.YieldAmounts.Add(new HiveYieldAmount()
            {
                Hive = hive,
                HiveId = hive.HiveId,
                Yield = yield1,
                YieldId = yield1.YieldId,
                Amount = 3,
                HoneycombCount = 9,

            });
            hive2.YieldAmounts.Add(new HiveYieldAmount()
            {
                Hive = hive2,
                HiveId = hive2.HiveId,
                Yield = yield1,
                YieldId = yield1.YieldId,
                Amount = 2,
                HoneycombCount = 3,
            });
            hive2.YieldAmounts.Add(new HiveYieldAmount()
            {
                Hive = hive2,
                HiveId = hive2.HiveId,
                Yield = yield2,
                YieldId = yield2.YieldId,
                Amount = 1.2,
                HoneycombCount = 2,
            });
            location.Hives.Add(hive);
            apiary.Locations.Add(location);
            location2.Hives.Add(hive2);
            apiary.Locations.Add(location2);
            context.Apiaries.Add(apiary);
            await context.SaveChangesAsync();
            
        }
    }

    internal class ApplicationRolesConfig
    {
        public ApplicationRoleConfig[] Roles { get; set; }
    }

    internal class ApplicationRoleConfig
    {
        public string Role { get; set; }
        public string Description { get; set; }
    }

    internal class SeedData
    {
        public string[] Roles { get; set; }
        public ApplicationSeedUser[] Users { get; set; }
    }

    public class ApplicationSeedUser : ApplicationUser
    {
        public string Role { get; set; }
    }
}