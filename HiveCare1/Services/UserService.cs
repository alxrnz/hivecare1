﻿using System.Security.Claims;
using Microsoft.AspNetCore.Http;

namespace HiveCare1.Services
{
    public class UserService
    {
        private readonly IHttpContextAccessor _context;
        public UserService(IHttpContextAccessor context)
        {
            _context = context;
        }

        public ClaimsPrincipal GetUser()
        {
            return _context.HttpContext?.User;
        }
    }
}