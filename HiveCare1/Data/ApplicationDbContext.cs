﻿using System;
using System.Security.Cryptography.X509Certificates;
using HiveCare1.Areas.Identity;
using HiveCare1.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace HiveCare1.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, string>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<Location> Locations { get; set; }
        public DbSet<Hive> Hives { get; set; }
        public DbSet<Feed> Feeds { get; set; }
        public DbSet<Diary> Diaries { get; set; }
        public DbSet<Process> Processes { get; set; }
        public DbSet<Observation> Observations { get; set; }
        public DbSet<Treatment> Treatments { get; set; }
        public DbSet<Yield> Yields { get; set; }
        public DbSet<ApplicationUser> ApplicationUsers { get; set; }
        public DbSet<Apiary> Apiaries { get; set; }
        public DbSet<HiveApplicationUser> HiveApplicationUsers { get; set; }
        public DbSet<HiveYieldAmount> HiveYieldAmounts { get; set; }
        public DbSet<ApiaryApplicationUser> ApiaryApplicationUsers { get; set; }
        public DbSet<LocationApplicationUser> LocationApplicationUsers { get; set; }
        public DbSet<Queen> Queens { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Apiary>()
                .HasMany(ap => ap.Locations)
                .WithOne(loc => loc.Apiary)
                .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<Location>()
                .HasOne(loc => loc.Apiary)
                .WithMany(apiary => apiary.Locations);

            modelBuilder.Entity<Hive>()
                .HasMany(hive => hive.YieldAmounts)
                .WithOne(yieldAmount => yieldAmount.Hive)
                .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<Yield>()
                .HasMany(yield => yield.Amounts)
                .WithOne(am => am.Yield);

            modelBuilder.Entity<HiveYieldAmount>()
                .HasOne(x => x.Yield)
                .WithMany(y => y.Amounts);

            modelBuilder.Entity<HiveYieldAmount>()
                .HasOne(x => x.Hive)
                .WithMany(y => y.YieldAmounts);

            #region Enum Conversion

            

           
            modelBuilder.Entity<Feed>().Property(e => e.FeedType)
                .HasMaxLength(50)
                .HasConversion(
                    v => v.ToString(),
                    v => (FeedTypeEnum)Enum.Parse(typeof(FeedTypeEnum), v))
                .IsUnicode(false);

            modelBuilder.Entity<Hive>().Property(e => e.Type)
                .HasMaxLength(50)
                .HasConversion(
                    v => v.ToString(),
                    v => (HiveTypeEnum)Enum.Parse(typeof(HiveTypeEnum), v))
                .IsUnicode(false);



            



            modelBuilder.Entity<Observation>().Property(e => e.Liningcells)
                .HasMaxLength(50)
                .HasConversion(
                    v => v.ToString(),
                    v => (AmountValueEnum)Enum.Parse(typeof(AmountValueEnum), v))
                .IsUnicode(false);
            modelBuilder.Entity<Process>().Property(e => e.FlightholeWedge)
                .HasMaxLength(50)
                .HasConversion(
                    v => v.ToString(),
                    v => (FlightholeStatusEnum)Enum.Parse(typeof(FlightholeStatusEnum), v))
                .IsUnicode(false);
            modelBuilder.Entity<Treatment>().Property(e => e.TypeOfAgenttype)
                .HasMaxLength(50)
                .HasConversion(
                    v => v.ToString(),
                    v => (TreatmentAgenttypeEnum)Enum.Parse(typeof(TreatmentAgenttypeEnum), v))
                .IsUnicode(false);

            modelBuilder.Entity<Yield>().Property(e => e.YieldType)
                .HasMaxLength(50)
                .HasConversion(
                    v => v.ToString(),
                    v => (YieldTypeEnum)Enum.Parse(typeof(YieldTypeEnum), v))
                .IsUnicode(false);

            #endregion

            #region Many To Many Mapping
            modelBuilder.Entity<HiveApplicationUser>()
                .HasKey(t => new { t.ApplicationUserId, t.HiveId });

            modelBuilder.Entity<HiveApplicationUser>()
                .HasOne(hiveApplicationUser => hiveApplicationUser.ApplicationUser)
                .WithMany(applicationUser => applicationUser.Hives);


            modelBuilder.Entity<HiveApplicationUser>()
                .HasOne(pt => pt.Hive)
                .WithMany(t => t.Users);


            modelBuilder.Entity<LocationApplicationUser>()
                .HasKey(t => new {t.LocationId, t.ApplicationUserId});

            modelBuilder.Entity<LocationApplicationUser>()
                .HasOne(pt => pt.ApplicationUser)
                .WithMany(p => p.Locations)
                .HasForeignKey(pt => pt.ApplicationUserId);

            modelBuilder.Entity<LocationApplicationUser>()
                .HasOne(pt => pt.Location)
                .WithMany(t => t.Users)
                .HasForeignKey(pt => pt.LocationId);

            modelBuilder.Entity<ApiaryApplicationUser>()
                .HasKey(t => new {t.ApiaryId, t.ApplicationUserId});

            modelBuilder.Entity<ApiaryApplicationUser>()
                .HasOne(pt => pt.ApplicationUser)
                .WithMany(p => p.Apiaries)
                .HasForeignKey(pt => pt.ApplicationUserId);

            modelBuilder.Entity<ApiaryApplicationUser>()
                .HasOne(pt => pt.Apiary)
                .WithMany(t => t.Users)
                .HasForeignKey(pt => pt.ApiaryId);

            #endregion
            

            
        }
    }
}