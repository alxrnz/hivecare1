FROM microsoft/dotnet:sdk AS build-env
WORKDIR /app

# Copy csproj and restore as distinct layers
COPY *.sln .
COPY HiveCare1/*.csproj ./HiveCare1/
RUN dotnet restore

# Copy everything else and build
COPY HiveCare1/. ./HiveCare1/
WORKDIR /app/HiveCare1
RUN dotnet publish -c Release -o out
#RUN dotnet ef migrations add InitialCreate -o out
#RUN dotnet ef database update -o out

# Build runtime image
FROM microsoft/dotnet:aspnetcore-runtime
WORKDIR /app
COPY --from=build-env /app/HiveCare1/out .
ENTRYPOINT ["dotnet", "HiveCare1.dll"]