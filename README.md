# Table of Contents

[TOC]

# Getting started

To run the application, the MS .Net Core Runtime needs to be installed. HiveCare is a .Net Core 2.2 application, so ensure a suitable runtime environment is present. Get the Files [here](https://dotnet.microsoft.com/download) and install like described on the Microsoftdotnet- Website.

After the installation of the runtime, it's recommended to install the corresponding SDK to update the database according to the current migration by invoking `dotnet ef migrations add <anyname>` .

## Setup the environment 

### Windows

Just install via Installer downloaded from link above and you're done with this step.

### Linux

### Ubuntu

Follow the instructions on the MS- Webiste. You'll only need to install the .Net Core SDK on Ubuntu, the Runtime comes besides.

#### Raspbian

1. Add symbolic link to the Runtime have faster access to dotnet commands: `sudo ln -s /opt/dotnet/dotnet /usr/local/bin` .

   This let's you start a dotnet app by invoking `dotnet /path/to/file.dll`.

2. Add symbolic link to the SDK `sudo ln -s $HOME/dotnet/dotnet /user/local/bin/dotnets`.

   This link enables you to build, publish, manage migrations and update the database

## Setup the project

1. clone or download and unzip the Repository. 

2. `cd <pathToRepo>/HiveCare1`

3. invoke `dotnet restore` to get the NuGet- Packages

4. invoke `dotnet build` to see if it works and get the client side dependencies

   ​		There is a huge load during development. Restoring (open-iconic) might take a while.

5. invoke `dotnet publish` to get the latest version composed and compiled

6.  `cd <pathToRepo>/HiveCare1/bin/Debug/netcoreapp2.2/publish`

7. invoke `dotnet ./HiveCare1.dll` 

8. You should see the following screen

   ```
   Hosting environment: Production
   Content root path: C:\Users\Steven\source\repos\HiveCare1\HiveCare1\bin\Debug\netcoreapp2.2\publish
   Now listening on: http://localhost:5000
   Now listening on: https://localhost:5001
   Application started. Press Ctrl+C to shut down.
   
   ```

If you see no map under Dashboard, build and publish again. Sometimes there are not all files downloaded and published.

## Setup with Docker

Run command in same folder as Dockerfile to generate image

docker build -t aspnetapp .

Run image via

 docker run -p5000:80 -p:5001:443 -d --name myapp aspnetapp
 
## Login

Default super user:

root@hivecare.io

Test123!
